var setPassWindow;

function showSetPassWindow()
{
    if(!setPassWindow)
    {
        setPassWindow = new Ext.Window({
            title: 'Registro de contraseña',
            width: 450,
            height:285,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [setPassWindowForm],
            closeAction :'hide'
        });
    }
    setPassWindowForm.form.reset();
     Ext.getCmp('eusr').setValue($F('usuario'));
    setPassWindow.show();
}
Ext.apply(Ext.form.VTypes, {


    password : function(val, field) {
        if (field.initialPassField) {
            var pwd = Ext.getCmp(field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },

    passwordText : 'Passwords no coinciden'
});
loginForm = function() {

    return {

        init : function(){

            setPassWindowForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                height:245,
                items: [
                {
                    xtype:'textfield',
                    inputType: 'password',
                    fieldLabel: 'Password',
                    name: 'pass',
                    id: 'pass',
                    maxLength : "10",
                    minLength:"10",
                    allowBlank: false
                },
                {
                    xtype:'textfield',
                    inputType: 'password',
                    fieldLabel: 'Confirmar Password',
                    name: 'pass-cfrm',
                    vtype: 'password',
                    maxLength : "10",
                    minLength:"10",
                    initialPassField: 'pass' // id of the initial password field
                    ,
                    allowBlank: false
                }
                ,
                {
                    id:'sq',
                    xtype:'combo',
                    width:250,
                    fieldLabel: 'Pregunta',
                    store: [['1','¿Cuál es su color favorito?'],['2','¿Cuál es el nombre de su mascota?'],['3','Nombre de cantante favorito.']],
                    typeAhead: true,
                    forceSelection: true,
                    emptyText:'Seleccione ...',
                    selectOnFocus:true,
                    triggerAction: 'all',
                    mode:'local'
                },
                {
                    width : 300,
                    xtype : "textarea",
                    name: "respuesta",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Respuesta",
                    allowBlank: false
                },
                {
                    id:'eusr',
                    xtype : "hidden",
                    name:'eusr'
                }
                ]
            });

            setPassWindowForm.addButton({
                text: 'Guardar',
                handler: function(){
                    setPassWindowForm.getForm().submit({
                        url:'securityController.htm?action=set',
                        waitMsg:'Guardando ...',

                        success: function( frm,act )
                        {
                            $('password').value= Ext.getCmp('pass').getValue();
                            send();
                            setPassWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });


            Ext.form.Field.prototype.msgTarget = 'under'

            var required = new Ext.form.TextField({
                allowBlank:false
                ,
                applyTo:'usuario'
            });

            var alpha = new Ext.form.TextField({
                allowBlank:false
                ,
                inputType: 'password'
                ,
                applyTo: 'password'
            });

            var login = new Ext.Action({
                text: 'Acceso'
                ,
                applyTo: 'button_login'
                ,
                handler: function(){
                    send();
                }
            //,iconCls: 'blist'
            });

            //            var loginbutton= new Ext.Button('button_login', {text:'login',     handler: this.send});
            var loginbutton = new Ext.Button(login);
            var formPanel = Ext.get("formPanel");
            formPanel.center();
        }

    };

}();


function success(response) {

    var result = Ext.decode(response.responseText);
    switch(result.status){
        case 'true':        window.location=result.value; break;
        case 'false':    {
            if(result.value=='SET')
                showSetPassWindow();
            else
                Ext.getDom('systemMsg').innerHTML=result.value;
        }
    }
};

function failure() {
    Ext.MessageBox.alert('Something went wrong');
};

function send() {
    //Seralizamos el formulario
    var params=Ext.Ajax.serializeForm('form1');
    var url = 'login.htm';
    Ext.Ajax.request({
        url:url
        ,
        params: params
        ,
        jmethod:'post'
        ,
        success: function(response){
            success(response)
        }
    }
    );
}

//Ext.EventManager.onDocumentReady(loginForm.init, loginForm, true);
Ext.onReady(loginForm.init, loginForm, true);