<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<head>
   
    <title>Almacen</title>
    <link rel="stylesheet" type="text/css" href="resources/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/form.css" />
    <link rel="stylesheet" type="text/css" href="grid-examples.css" />
    <script type="text/javascript" src="prototype-1.6.0.3.js"></script>

    <script type="text/javascript" src="adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="ext-all.js"></script>
    <script type="text/javascript">
        Ext.form.Checkbox.override({
            setValue : function(v) {
                var checked = this.checked;
                this.checked = (v === true || v === 'true' || v == '1' || String(v).toLowerCase() == 'on');

                if(this.rendered){
                    this.el.dom.checked = this.checked;
                    this.el.dom.defaultChecked = this.checked;
                    this.wrap[this.checked? 'addClass' : 'removeClass'](this.checkedCls);
                }

                if(checked != this.checked){
                    this.fireEvent("check", this, this.checked);
                    if(this.handler){
                        this.handler.call(this.scope || this, this, this.checked);
                    }
                }
            },

            afterRender : function(){
                Ext.form.Checkbox.superclass.afterRender.call(this);
                this.wrap[this.checked? 'addClass' : 'removeClass'](this.checkedCls);
            }
        });
    </script>
    <script type="text/javascript" src="ext-lang-es.js"></script>
    <script type="text/javascript" src="manager.js"></script>

    <style type="text/css">

                                                  #tree{

                                                      margin:0px;
                                                      border:0px;
                                                      height:2048px;
                                                  }
                                         
                                                  celda {color:green;}

                                                  html, body {
                                                      font:normal 12px verdana;
                                                      margin:0;
                                                      padding:0;
                                                      border:0 none;
                                                      overflow:hidden;
                                                      height:100%;
                                                  }
                                                  p {
                                                      margin:5px;
                                                  }
                                                  .settings {
                                                      background-image:url(shared/icons/fam/folder_wrench.png);
                                                  }
                                                  .nav {
                                                      background-image:url(shared/icons/fam/folder_go.png);
                                                  }


    </style>
    <script type="text/javascript">
        Ext.onReady(function(){
            Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

            var viewport = new Ext.Viewport({
                layout:'border',
                items:[
                    {
                        region:'west',
                        id:'west-panel',
                        title:'Control',
                        split:true,
                        width: 250,
                        minSize: 250,
                        maxSize: 250,
                        collapsible: true,
                        margins:'0 0 0 5',
                        layout:'accordion',

                        layoutConfig:{
                            animate:true
                        },
                        items: [{
                                contentEl: 'west',
                                title:'Arbol',
                                border:false,
                                iconCls:'nav'
                            }]
                    },


                    {
                        region:'south',
                       
                        split:true,
                        height: 550,
                        layout:'fit',
                        minSize: 100,
                        maxSize: 200,
                        collapsible: true,
                        title:' ',
                        margins:'0 0 0 0',
                        items:[
                            {

                                xtype:'form',
                                frame: true,
                                labelAlign: 'left',
                                title: 'Histórico',
                                bodyStyle:'padding:5px',
                                width: 750,
                                layout: 'column',
                                items: [{
                                        columnWidth: 0.6,
                                        layout: 'fit',
                                        items: {

                                            xtype: 'grid',
                                            layout: 'fit',
                                            store: histoStore,
 bbar:new Ext.PagingToolbar({    
        pageSize: 25,
        store: histoStore,
        displayInfo: true,
        displayMsg: 'Mostrando resultados {0} - {1} de {2}',
        emptyMsg: "no hay resultados que mostrar"

    }),
                                            stripeRows : true,
                                            viewConfig: {
                                                forceFit: true
                                            },
                                            columns: [
                                                {
                                                    header: "Tipo",
                                                    width: 30,
                                                    dataIndex: 'item',
                                                    sortable: true
                                                },
                                                {
                                                    header: "Codigo",
                                                    width: 90,
                                                    dataIndex: 'clave',
                                                    sortable: true
                                                },
                                                {
                                                    header: "Descripción",
                                                    width: 180,
                                                    dataIndex: 'descripcion',
                                                    sortable: true
                                                },

                                                {
                                                    header: "Usuario",
                                                    width: 30,
                                                    dataIndex: 'usuario',
                                                    sortable: true
                                                },
                                                {
                                                    header: "Accion",
                                                    width: 40,
                                                    dataIndex: 'accion',
                                                    sortable: true
                                                },
                                                {
                                                    header: "Fecha",
                                                    width: 30,
                                                    dataIndex: 'fecha',
                                                    sortable: true
                                                }
                                            ],
                                            sm: new Ext.grid.RowSelectionModel({
                                                singleSelect: true,
                                                listeners:
                                                    {

                                                    rowselect: function( selModel , index , data )
                                                    {

                                                    }
                                                }
                                            }),
                                            width:800,
                                            height:400


                                        }
                                    },{

                                        columnWidth: 0.4,
                                        xtype: 'fieldset',
                                        labelWidth: 90,
                                        title:'Filtro',
                                        defaults: {width: 140},	// Default config options for child items
                                       
                                        autoHeight: true,
                                        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                                        border: false,
                                        style: {
                                            "margin-left": "10px", // when you add custom margin in IE 6...
                                            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
                                        },
                                        items: [{
                                                id:'tc',
                                                xtype:'combo',
                                                
                                                fieldLabel: 'Tipo',
                                                store: [['%','Todos'],['DEPARTAMENTO','Departamento'],['LINEA','Linea'],['MAQUINA','Máquina'],['PARTE','Parte']],
                                                typeAhead: true,
                                                forceSelection: true,
                                                emptyText:'Seleccione ...',
                                                selectOnFocus:true,
                                                triggerAction: 'all',
                                                mode:'local'
                                            },userTxt={
                                                xtype:'textfield',
                                                id:'usuario',
                                                fieldLabel: 'Usuario',
                                                name: 'usuario',
                                                value:'%'
                                            },{
                                                id:'ac',
                                                xtype:'combo',
                                                forceSelection: true,
                                                triggerAction: 'all',
                                                fieldLabel: 'Acción',
                                                store: [['%','Todas'],['ACTUALIZACION','Actualización'],['BORRADO','Borrado'],['AGREGAR','Agregar'],['NUEVO','Nuevo'],['SACAR','Sacar']],
                                                typeAhead: true,
                                                forceSelection: true,
                                                emptyText:'Seleccione ...',
                                                selectOnFocus:true,
                                                mode:'local'
                                            },{id:'fi',

                                                xtype: 'datefield',
                                                fieldLabel: 'Fecha inicial',
                                                name: 'fechaInicial'
                                            },
                                            {
                                                id:'ff',

                                                xtype: 'datefield',
                                                fieldLabel: 'Fecha final',
                                                name: 'fechaFinal'
                                            },{xtype:'button',text:'Consultar',handler:doHisto}]
                                    }]

                            }
                        ]}


                    ,
                    new Ext.TabPanel({

                        region:'center',
                        contentEl: 'center1',
                        deferredRender:false,
                        activeTab:0,
                        items:[
                            {
                                title: 'Departamentos',
                                cls: 'inner-tab-custom',
                                layout: 'border',
                                hideMode: Ext.isIE ? 'offsets' : 'display',
                                items: [
                                    deptosGrid={
                                        region: 'center',
                                        xtype: 'grid',
                                        layout: 'fit',
                                        store: deptosStore,
                                        loadMask:true,
                                        tbar:
                                            new Ext.Toolbar([
                                            {icon: 'resources/images/my/add.png',cls: 'x-btn-icon',tooltip : "Añadir", handler:showNewDeptoWindow},
                                            {xtype: 'tbseparator'},
                                            {icon: 'resources/images/my/delete.png',cls: 'x-btn-icon',tooltip : "Eliminar",handler:showDeleteDeptoWindow},
                                            {xtype: 'tbseparator'}
                                            ,
                                            {icon: 'resources/images/my/pencil.png',cls: 'x-btn-icon',tooltip : "Editar",handler:showEditDeptoWindow}
                                        ]),
                                        stripeRows : true,
                                        viewConfig: {forceFit: true},

                                        columns: [
                                            {header: "ID",width: 50,dataIndex: 'id',sortable: true, align:'right'},
                                            {header: "Descripción", width: 250,dataIndex: 'descripcion',sortable: true,align:'left'},
                                            {header: "Creación", width: 70,dataIndex: 'creacion',sortable: true,align:'left'},
                                            {header: "Actualización", width: 70,dataIndex: 'actualizacion',sortable: true,align:'left'},
                                            {header: "Lineas",width: 100,dataIndex: 'lineas',sortable: true,align:'left'}
                                        ],

                                        sm: new Ext.grid.RowSelectionModel({
                                            singleSelect: true,
                                            listeners:
                                                {

                                                rowselect: function( selModel , index , data )
                                                {
                                                    /* if(depto_to_load!=null)
                                                        askDepartament=depto_to_load;
                                                    else
                                                    {
                                                        var lineas=data.get('lineas');
                                                        if(lineas!=0)
                                                        {
                                                            var departamento=data.get('id');
                                                            askDepartament=departamento;
                                                        }
                                                        else
                                                        {
                                                            askDepartament=none;
                                                        }
                                                    }
                                                     */
                                                    var lineas=data.get('lineas');
                                                    if(lineas!=0)
                                                    {

                                                        askDepartament=data.get('id');
                                                    }
                                                    else
                                                    {
                                                        askDepartament=none;
                                                    }

                                                    lineasStore.reload({params:{departamento: askDepartament},add:false});

                                                }
                                            }
                                        }),
                                        width:640,
                                        height:200

                                    }

                                    ,
                                    {
                                        xtype: 'tabpanel',
                                        plain: true,
                                        region: 'south',

                                        activeTab: 0,
                                        split: true,

                                        items: [
                                            {

                                                title: 'Lineas',
                                                cls: 'inner-tab-custom',
                                                layout: 'border',
                                                hideMode: Ext.isIE ? 'offsets' : 'display',
                                                items: [
                                                    {
                                                        xtype: 'tabpanel',
                                                        region: 'center',

                                                        tabPosition: 'bottom',
                                                        activeTab: 0,
                                                        items: [lineasGrid1]
                                                    }

                                                ]
                                            }
                                        ]
                                    }

                                ]
                            }
                            ,
                            {

                                title: 'Lineas',
                                cls: 'inner-tab-custom',
                                layout: 'border',

                                hideMode: Ext.isIE ? 'offsets' : 'display',
                                items: [

                                    lineasGrid2={
                                        region: 'center',
                                        xtype: 'grid',
                                        layout: 'fit',
                                        store: lineasStore,
                                        tbar:
                                            new Ext.Toolbar([
                                            {icon: 'resources/images/my/add.png',cls: 'x-btn-icon',tooltip : "Añadir",handler:showNewLineWindow },
                                            {xtype: 'tbseparator'},
                                            {id:'dl2',icon: 'resources/images/my/delete.png',cls: 'x-btn-icon',toltip:"Eliminar",handler:showDeleteLineaWindow},
                                            {xtype: 'tbseparator'},
                                            {id:'el2',icon: 'resources/images/my/pencil.png',cls: 'x-btn-icon',tooltip : "Editar",handler:showEditLineWindow}
                                        ]),

                                        stripeRows : true,
                                        viewConfig: {forceFit: true},
                                        columns: [
                                            {header: "TCC",width: 15,dataIndex: 'tcc',sortable: true},
                                            {header: "Descripción",width: 180, dataIndex: 'descripcion', sortable: true },
                                            {header: "Creación", width: 70,dataIndex: 'creacion',sortable: true,align:'left'},
                                            {header: "Actualización", width: 70,dataIndex: 'actualizacion',sortable: true,align:'left'},
                                            {header: "Maquinas",width: 115,dataIndex: 'maquinas', sortable: true}
                                        ],

                                        sm: lineasGrid2Sm,
                                        width:640,
                                        height:200
                                    },
                                    {
                                        xtype: 'tabpanel',
                                        plain: true,
                                        region: 'south',

                                        activeTab: 0,
                                        split: true,

                                        items: [
                                            {

                                                title: 'Máquinas',
                                                cls: 'inner-tab-custom',
                                                layout: 'border',
                                                hideMode: Ext.isIE ? 'offsets' : 'display',
                                                items: [
                                                    {
                                                        xtype: 'tabpanel',
                                                        region: 'center',

                                                        tabPosition: 'bottom',
                                                        activeTab: 0,
                                                        items: [maquinasGrid1]
                                                    }

                                                ]
                                            }
                                        ]
                                    }


                                ]
                            },
                            maquinasGrid2=
                                {
                                title: 'Máquinas',
                                cls: 'inner-tab-custom',
                                layout: 'border',

                                hideMode: Ext.isIE ? 'offsets' : 'display',
                                items: [

                                    {
                                        region: 'center',
                                        xtype: 'grid',
                                        layout: 'fit',
                                        store: maquinasStore,
                                        tbar:
                                            new Ext.Toolbar([
                                            {icon: 'resources/images/my/add.png',cls: 'x-btn-icon',tooltip : "Añadir",handler:showNewMachineWindow},
                                            {xtype: 'tbseparator'},
                                            {icon: 'resources/images/my/delete.png',cls: 'x-btn-icon', tooltip : "Eliminar",handler:showDeleteMachineWindow},
                                            {xtype: 'tbseparator'},
                                            {icon: 'resources/images/my/pencil.png',cls: 'x-btn-icon',tooltip : "Editar",handler:showEditMachineWindow},
                                            {
                                                xtype: 'tbsplit',
                                                text: 'ESTADO',

                                                menu: new Ext.menu.Menu({
                                                    items: [
                                                        // These items will display in a dropdown menu when the split arrow is clicked
                                                        {
                                                            text: 'ACTIVA',
                                                            checked: true,
                                                            group: 'theme',
                                                            handler:function(){
                                                                askStatus='A';maquinasStore.reload({
                                                                    params:{
                                                                        linea: askLine,
                                                                        estado:askStatus
                                                                    },
                                                                    add:false
                                                                });
                                                            }
                                                        },
                                                        {
                                                            text: 'INACTIVA',
                                                            checked: false,
                                                            group: 'theme',
                                                            handler:function(){
                                                                askStatus='I';maquinasStore.reload({
                                                                    params:{
                                                                        linea: askLine,
                                                                        estado:askStatus
                                                                    },
                                                                    add:false
                                                                });
                                                            }
                                                        }
                                                    ]
                                                })
                                            }          ]),

                                        stripeRows : true,
                                        columns: [
                                            {header: "ID",width: 10,dataIndex: 'id',sortable: true},
                                            {header: "Descripción",width: 60, dataIndex: 'descripcion',sortable: true},
                                            {header: "Descripción corta",width: 60,dataIndex: 'corta',sortable: true},
                                            {header: "Estado", width: 20, dataIndex: 'estado',sortable: true},
                                            {header: "Creación", width: 70,dataIndex: 'creacion',sortable: true,align:'left'},
                                            {header: "Actualización", width: 70,dataIndex: 'actualizacion',sortable: true,align:'left'},
                                            {header: "Partes",width: 20,dataIndex: 'partes', sortable: true}
                                        ],
                                        viewConfig: {
                                            forceFit: true
                                        },
                                        sm:maquinasGrid2Sm ,
                                        width:640,
                                        height:200
                                    },

                                    {
                                        xtype: 'tabpanel',
                                        plain: true,
                                        region: 'south',

                                        activeTab: 0,
                                        split: true,

                                        items: [
                                            {

                                                title: 'Partes',
                                                cls: 'inner-tab-custom',
                                                layout: 'border',
                                                hideMode: Ext.isIE ? 'offsets' : 'display',
                                                items: [
                                                    {
                                                        xtype: 'tabpanel',
                                                        region: 'center',

                                                        tabPosition: 'bottom',
                                                        activeTab: 0,
                                                        items: [
                                                            partesGrid={
                                                                region: 'center',
                                                                xtype: 'grid',
                                                                layout: 'fit',
                                                                store: partesStore,
                                                               
                                                                tbar:
                                                                    new Ext.Toolbar([
                                                                    {icon: 'resources/images/my/add.png',cls: 'x-btn-icon',tooltip : "Añadir",handler:showNewPartWindow},
                                                                    {xtype: 'tbseparator'},
                                                                    {icon: 'resources/images/my/delete.png',cls: 'x-btn-icon',toltip:"Eliminar",handler:showDeletePartWindow},
                                                                    {icon: 'resources/images/my/pencil.png',cls: 'x-btn-icon',tooltip : "Editar",handler:showEditPartWindow},
                                                                    {icon: 'resources/images/my/search.png',cls: 'x-btn-icon',tooltip:"Buscar",handler:showSearchPartWindow}

                                                                ]),

                                                                stripeRows : true,
                                                                viewConfig: {
                                                                    forceFit: true
                                                                },
                                                                columns: [
                                                                    {
                                                                        header: "Número",
                                                                        width: 20,
                                                                        dataIndex: 'numero',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Descripción",
                                                                        width: 180,
                                                                        dataIndex: 'descripcion',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Descripción corta",
                                                                        width: 180,
                                                                        dataIndex: 'corta',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Cantidad",
                                                                        width: 90,
                                                                        dataIndex: 'cantidad',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Caja",
                                                                        width: 20,
                                                                        dataIndex: 'caja',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Posicion",
                                                                        width: 20,
                                                                        dataIndex: 'posicion',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Fila",
                                                                        width: 20,
                                                                        dataIndex: 'fila',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Anaquel",
                                                                        width: 20,
                                                                        dataIndex: 'anaquel',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Creación",
                                                                        width: 20,
                                                                        dataIndex: 'creacion',
                                                                        sortable: true
                                                                    },
                                                                    {
                                                                        header: "Actualización",
                                                                        width: 20,
                                                                        dataIndex: 'actualizacion',
                                                                        sortable: true
                                                                    }
                                                                ],
                                                                sm: new Ext.grid.RowSelectionModel({
                                                                    singleSelect: true,
                                                                    listeners:
                                                                        {

                                                                        rowselect: function( selModel , index , data )
                                                                        {

                                                                        }
                                                                    }
                                                                }),
                                                                width:640,
                                                                height:200


                                                            }]
                                                    }

                                                ]
                                            }
                                        ]
                                    }

                                ]
                            }


                        ]
                    })
                ]
            });
            /*   Ext.get("hideit").on('click', function() {
           var w = Ext.getCmp('west-panel');
           w.collapsed ? w.expand() : w.collapse();
        });
             */
        });
    </script>
</head>
<body>

    <div id="west" >

        <div id="tree" style="overflow:auto; height:600px;width:250px;border:1px solid #c3daf9;"></div>
    </div>


    <div id="center1" class="grid-example" >
    </div>


    <div id="center2" class="grid-example" align="center">
    </div>
    <div id="props-panel" style="width:195px;height:300px;overflow:hidden;">
    </div>
    <div id="south">

    </div>


</body>
</html>



