<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="resources/css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="resources/css/form.css" />
         <script type="text/javascript" src="prototype-1.6.0.3.js"></script>
        <script type="text/javascript" src="adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="ext-all.js"></script>
        <script type="text/javascript" src="ext-lang-es.js"></script>
        <script type="text/javascript" src="login2.js"></script>
        <style type="text/css">
            #x {
                position:absolute;
                top: 50%;
                left: 50%;
                width:18em;
                height:8em;
                margin-top: -4em; /*set to a negative number 1/2 of your height*/
                margin-left: -9em; /*set to a negative number 1/2 of your width*/
                border: 1px solid #ccc;
                background-color: #f3f3f3;
            }

        </style>
    </head>
    <body background="resources/images/my/main.png">
        <div id="x">
            <form id="form1" class="x-form">
                <input name="hidden" type="hidden" value="" />
                <div  id="formPanel" style="width:288px;">
                    <div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>
                    <div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc">
                                <h3 style="margin-bottom:5px;">Login</h3>
                                <!-- begin form layout -->
                                <div class="x-form-bd" id="container">
                                    <div class="x-form-item">
                                        <label for="required">Usuario:</label>
                                        <div class="x-form-element">
                                            <input type="text" size="24" name="usuario" id="usuario" />
                                        </div>
                                    </div>
                                    <div class="x-form-item">
                                        <label for="alpha">Password:</label>
                                        <div class="x-form-element">
                                            <input type="password" size="24" name="password" id="password" />
                                        </div>
                                    </div>
                                    <div id="button_login"></div>
                                </div>
                                <!-- end form layout -->
                    </div></div></div>
                    <div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>
                    <div id="systemMsg"></div>
                </div>
            </form>
        </div>
    </body>
</html>