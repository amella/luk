var Tree = Ext.tree;
var emb=Ext.MessageBox;
var tree ;
var root;
var myMask ;
var deptosGrid;
var deptosStore;
var lineasGrid1;
var lineasGrid2;
var lineasGrid2Sm;
var lineasStore;
var maquinasGrid1;
var maquinasGrid2;
var maquinasGrid2Sm;
var maquinasStore;
var partesStore;
var partesGrid;
var askDepartament=escape('%');
var askLine='w';
var espacio=' ';
var none='noneAll' ;
var askStatus='A';
var first=true;
var askMachine=null;

var sacarPartWindow;
var meterPartWindow;
var searchPartWindow;
var depto_to_load=null;
var tcc_to_load=null;
var maq_to_load=null;
var part_to_load=null;


function showMeterPartWindow()
{
    if(!meterPartWindow)
    {   
        meterPartWindow = new Ext.Window({
            title: 'Agregar cantidad',
            width: 250,
            height:150,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [meterPartForm],
            closeAction :'hide'
        });
    }
    var mrecord=maquinasGrid1.getSelectionModel().getSelected();
    var record=partesGrid.sm.getSelected();
    if(!record) return;
    meterPartForm.form.reset();
   
    Ext.getCmp( "mpfcaja").setValue(record.get('caja'));
    Ext.getCmp( "mpfposicion").setValue(record.get('posicion'));

    Ext.getCmp('mpMaquina').setValue(mrecord.get('id'));
  
    meterPartWindow.show();
}




function showSacarPartWindow()
{
    if(!sacarPartWindow)
    {
        sacarPartWindow = new Ext.Window({
            title: 'Retirar cantidad',
            width: 250,
            height:150,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [sacarPartForm],
            closeAction :'hide'
        });
    }
    var mrecord=maquinasGrid1.getSelectionModel().getSelected();
    var record=partesGrid.sm.getSelected();
    if(!record) return;
    sacarPartForm.form.reset();
    
    Ext.getCmp( "spfcaja").setValue(record.get('caja'));
    Ext.getCmp( "spfposicion").setValue(record.get('posicion'));
  
    Ext.getCmp('spMaquina').setValue(mrecord.get('id'));
   


    sacarPartWindow.show();
}

function showSearchPartWindow()
{

    if(!searchPartWindow)
    {
        searchPartWindow = new Ext.Window({
            title: 'Buscar Parte',
            width: 902,
            height:390,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [searchPartForm],
            closeAction :'hide'
        });
    }
    searchStore.reload({params:{d:' ',c:' '},add:false});
    searchPartForm.form.reset();
    searchPartWindow.show();
}

function doParte()
{

var desc=Ext.getCmp('sfDesc').getValue();
var desccorta=Ext.getCmp('sfDescCorta').getValue();

    desc=desc.replace(/%/, '');
    desccorta=desccorta.replace(/%/, '');

if((desc.length==0)&&(desccorta.length==0))
    return;

searchStore.reload({params:{d:desc,c:desccorta},add:false});
}


var TreeTest = function()
{
    return {
        init : function()
        {
         searchStore= new Ext.data.JsonStore
                         (
                          { 
                            url: 'parteController.htm?action=search',
                            root:'parte',
                            fields: [
                                     {name:'departamento',type:'string'},
                                     {name:'linea',type:'string'},
                                     {name:'maquina',type:'string'},
                                     {name:'descripcion',type:'string'},
                                     {name:'corta',type:'string'},
                                     {name:'cantidad',type:'int'},
                                     {name:'caja',type:'string'},
                                     {name:'posicion',type:'string'},
                                     {name:'fila',type:'string'},
                                     {name:'anaquel',type:'string'}
                                    ]
                          }
                        );

         deptosStore = new Ext.data.JsonStore
                       (
                        {
                            url: 'departamentoController.htm?action=departamentosParaTabla',
                            root:'departamento',
                            fields: [
                                      {name:'id',type:'string' },
                                      {name:'descripcion',type:'string' },
                                      {name:'creacion',type:'string' },
                                      {name:'actualizacion',type:'string' },
                                      {name:'lineas',type:'int'}
                                    ]
                        }
                       );

            deptosStore.on('load' ,function(store, records,  options)
            {
                if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;
                    askDepartament=none;
                    lineasStore.reload({params:{departamento: askDepartament},add:false});
                    return;
                }

                var indice=0;

                if(depto_to_load!=null)
                {
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('id')==depto_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }
                deptosGrid.sm.selectRow( (indice), true );
            });

            lineasStore = new Ext.data.JsonStore({

                url: 'lineaController.htm?action=lineasParaTabla',
                root:'linea',
                fields: [
                        {name:'tcc', type:'string'},

                {
                    name:'descripcion',
                    type:'string'
                },

                {
                    name:'creacion',
                    type:'string'
                },

                {
                    name:'actualizacion',
                    type:'string'
                },

                {
                    name:'maquinas',
                    type:'int'
                }
                ]
            });

            lineasStore.on('load' ,function(store, records,  options)
            {


                if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;
                    askLine=none;
                      maquinasStore.reload({
                            params:{
                                linea: askLine,
                                estado: 'J'
                            },
                            add:false
                        });

                    return;
                }

                var indice=0;

                if(tcc_to_load!=null)
                {
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('tcc')==tcc_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }

                lineasGrid2Sm.selectRow( indice, true );

            });

            maquinasStore = new Ext.data.JsonStore({

                url: 'maquinaController.htm?action=maquinasParaTabla',
                root:'maquina',
                fields: [
                {
                    name:'id',
                    type:'string'
                },

                {
                    name:'descripcion',
                    type:'string'
                },

                {
                    name:'corta',
                    type:'string'
                },

                {
                    name:'estado',
                    type:'string'
                },

                {
                    name:'creacion',
                    type:'string'
                },

                {
                    name:'actualizacion',
                    type:'string'
                },

                {
                    name:'partes',
                    type:'int'
                }
                ]
            });

            maquinasStore.on('load' ,function(store, records,  options)
            {


                if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;

                    askMachine=none;
                    partesStore.reload({
                        params:{
                            maquina: askMachine
                        },
                        add:false
                    });
                    return;
                }

                lineasGrid2.sm.resumeEvents();
                lineasGrid1.getSelectionModel().resumeEvents();
                 var indice=0;

                if(maq_to_load!=null)
                {
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('id')==maq_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }



                    maquinasGrid2Sm.selectRow( indice, true );

            });

            partesStore = new Ext.data.JsonStore({
                url: 'parteController.htm?action=partesParaTabla',
                root:'parte',
                fields: [
                {
                    name:'numero',
                    type:'string'
                },

                {
                    name:'descripcion',
                    type:'string'
                },

                {
                    name:'cantidad',
                    type:'int'
                },

                {
                    name:'caja',
                    type:'string'
                },

                {
                    name:'posicion',
                    type:'string'
                },

                {
                    name:'fila',
                    type:'string'
                },

                {
                    name:'anaquel',
                    type:'string'
                },

                {
                    name:'creacion',
                    type:'string'
                },

                {
                    name:'actualizacion',
                    type:'string'
                }
                ]
            });

            partesStore.on('load' ,function(store, records,  options)
            {

               if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;
return;
                }

                maquinasGrid2Sm.resumeEvents();
                maquinasGrid1.getSelectionModel().resumeEvents();

var indice=0;

                if(part_to_load!=null)
                {
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('numero')==part_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }

                    partesGrid.sm.selectRow( indice, true );

                depto_to_load=null;
                tcc_to_load=null;
                maq_to_load=null;
                part_to_load=null;
            });


            lineasGrid2Sm=new Ext.grid.RowSelectionModel(
            {
                singleSelect: true,
                listeners:{
                    rowselect: function( selModel , index , data )
                    {
                        lineasGrid1.getSelectionModel().suspendEvents();
                        lineasGrid1.getSelectionModel().selectRow(index);
                        askLine=data.get('tcc');
                        maquinasStore.reload({
                            params:{
                                linea: askLine,
                                estado: askStatus
                            },
                            add:false
                        });
                    }
                }
            }
            );

            maquinasGrid2Sm=new Ext.grid.RowSelectionModel(
            {
                singleSelect: true,
                listeners:
                {
                    rowselect   : function( selModel , index , data )
                    {

                        maquinasGrid1.getSelectionModel().suspendEvents();
                        maquinasGrid1.getSelectionModel().selectRow(index);
                        askMachine=data.get('id');
                        partesStore.reload({
                            params:{
                                maquina: askMachine
                            },
                            add:false
                        });
                    }
                }
            }
            );
 meterPartForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {

                    id:'mpfcantidad',
                    name : "cantidad",
                    width : 60,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1,5}/,
                    regexText:"Número de X[XXXX] digitos.",
                    maxLength : "5",
                    maxLengthText : "Número de X[XXXX] digitos.",
                    minLengthText : "Número de X[XXXX] digitos.",
                    minLength : "1",
                    fieldLabel : "Cantidad",
                    allowBlank: false
                }
                ,
                {
                    id:'mpMaquina',
                    xtype : "hidden",
                    name:'maquina'
                },
                {
                    id:'mpfcaja',
                    xtype : "hidden",
                    name:'caja'
                },
                {
                    id:'mpfposicion',
                    xtype : "hidden",
                    name:'posicion'
                }
                ]
            });

            meterPartForm.addButton({
                text: 'Guardar',
                handler: function(){
                    meterPartForm.getForm().submit({
                        url:'parteController.htm?action=agregar',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            var depto_record=deptosGrid.sm.getSelected();
                            var line_record=lineasGrid2Sm.getSelected();
                            var maq_record=maquinasGrid2Sm.getSelected();
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=line_record.get('tcc');
                            maq_to_load=maq_record.get('id');
                            part_to_load=maq_to_load+meterPartForm.getForm().findField('mpfcaja').getValue()+meterPartForm.getForm().findField('mpfposicion').getValue();

                            root.reload();

                            meterPartWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

            sacarPartForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {

                    id:'spfcantidad',
                    name : "cantidad",
                    width : 60,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1,5}/,
                    regexText:"Número de X[XXXX] digitos.",
                    maxLength : "5",
                    maxLengthText : "Número de X[XXXX] digitos.",
                    minLengthText : "Número de X[XXXX] digitos.",
                    minLength : "1",
                    fieldLabel : "Cantidad",
                    allowBlank: false
                }                
                ,
                {
                    id:'spMaquina',
                    xtype : "hidden",
                    name:'maquina'
                },
                {
                    id:'spfcaja',
                    xtype : "hidden",
                    name:'caja'
                },
                {
                    id:'spfposicion',
                    xtype : "hidden",
                    name:'posicion'
                }
                ]
            });

            sacarPartForm.addButton({
                text: 'Guardar',
                handler: function(){
                    sacarPartForm.getForm().submit({
                        url:'parteController.htm?action=sacar',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            var depto_record=deptosGrid.sm.getSelected();
                            var line_record=lineasGrid2Sm.getSelected();
                            var maq_record=maquinasGrid2Sm.getSelected();
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=line_record.get('tcc');
                            maq_to_load=maq_record.get('id');
                            part_to_load=maq_to_load+sacarPartForm.getForm().findField('spfcaja').getValue()+sacarPartForm.getForm().findField('spfposicion').getValue();

                            root.reload();

                            sacarPartWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });


searchPartForm=new Ext.FormPanel(
           {
                                frame: true,
                                labelAlign: 'left',
                                title: 'Información',
                                bodyStyle:'padding:5px',
                                width: 875,
                                layout: 'column',
                                items: [{
                                        columnWidth: 0.7,
                                        layout: 'fit',
                                        items: {

                                            xtype: 'grid',
                                            layout: 'fit',
                                           store: searchStore,
                                           loadMask : true,
                                            stripeRows : true,
                                            viewConfig: {
                                                forceFit: true
                                            },
                                            columns: [
                                                {header: "Departamento",width: 20,dataIndex: 'departamento',sortable: true },
                                                {header: "Linea",width: 20,dataIndex: 'linea',sortable: true},
                                                {header: "Máquina",width: 90,dataIndex: 'maquina',sortable: true},
                                                {header: "Descripción",width: 90,dataIndex: 'descripcion',sortable: true},
                                                {header: "Descripcion Corta",width: 90,dataIndex: 'corta',sortable: true},
                                                {header: "Caja",width: 20,dataIndex: 'caja',sortable: true},
                                                {header: "Posicion",width: 20,dataIndex: 'posicion',sortable: true},
                                                {header: "Fila",width: 20,dataIndex: 'fila',sortable: true},
                                                {header: "Anaquel",width: 20,dataIndex: 'anaquel',sortable: true}
                                            ],
                                            sm: new Ext.grid.RowSelectionModel({
                                                singleSelect: true,
                                                listeners:
                                                    {

                                                    rowselect: function( selModel , index , data )
                                                    {

                                                    }
                                                }
                                            }),
                                            width:800,
                                            height:300


                                        }
                                    },{

                                        columnWidth: 0.3,
                                        xtype: 'fieldset',
                                        labelWidth: 90,
                                        title:'Filtro',
                                        defaults: {width: 120},	// Default config options for child items
                                        //defaultType: 'textfield',
                                        autoHeight: true,
                                        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                                        border: false,
                                        style: {
                                            "margin-left": "10px", // when you add custom margin in IE 6...
                                            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
                                        },
                                        items: [
                                            {
                                                id:'sfDesc',
                                                xtype:'textfield',
                                                fieldLabel: 'Descripción',
                                                name: 'sfDesc'

                                            },
                                             {
                                                id:'sfDescCorta',
                                                xtype:'textfield',
                                                fieldLabel: 'Descripción corta',
                                                name: 'sfDescCorta'

                                            },
                                            {xtype:'button',text:'Consultar',handler:doParte}]
                                    }]

                            }
            );




            Ext.QuickTips.init();

            tree= new Tree.TreePanel({
                el:'tree',
                animate:true,
                autoScroll:true,
                animCollapse : true,
                trackMouseOver : true,
                containerScroll:true,
                autoHeight:true
            });
            tree.on('dblclick',function(node, e)
            {

                var nId=node.id;
                if(nId=='main') return;
                if(nId.length==2)
                {
                    depto_to_load=nId;
                    deptosStore.reload({add:false});
                }
                else
                if(nId.length==8)
                {
                    maq_to_load=nId;
                    tcc_to_load=node.parentNode.id;
                    depto_to_load=node.parentNode.parentNode.id;
                     deptosStore.reload({add:false});
                }
                else
                if(nId.length==11)
                {

                    part_to_load=nId;
                    maq_to_load=node.parentNode.id;
                    tcc_to_load=node.parentNode.parentNode.id;
                    depto_to_load=node.parentNode.parentNode.parentNode.id;
                    deptosStore.reload({add:false});
                }
                else
                {
                    tcc_to_load=nId;
                    depto_to_load=node.parentNode.id;
                    deptosStore.reload({add:false});
                }

            });

            tree.on('load',function()
            {
                if(first)
                {
                    deptosGrid.store.reload({
                        add:false
                    });
                    first=false;
                }
            }
            );

            root= new Tree.AsyncTreeNode({
                id:'main',
                text:'Departamentos',
                leaf:false,
                loader:new Tree.TreeLoader({
                    dataUrl:'arbol.htm'
                })
                });
            root.on('load',function()
            {
                if(depto_to_load!=null)
                {
                    deptosGrid.store.reload({
                        add:false
                    });
                }

            });

            tree.setRootNode(root);
            tree.render();
            tree.syncSize();
            root.expand(false,false);

            lineasGrid1= new Ext.grid.GridPanel(
            {
                store: lineasStore,
                stripeRows : true,
                loadMask:false,
                columns: [
                {
                    header: "TCC",
                    width: 15,
                    dataIndex: 'tcc',
                    sortable: true
                },

                {
                    header: "Descripción",
                    width: 180,
                    dataIndex: 'descripcion',
                    sortable: true
                },

                {
                    header: "Creación",
                    width: 70,
                    dataIndex: 'creacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Actualización",
                    width: 70,
                    dataIndex: 'actualizacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Maquinas",
                    width: 115,
                    dataIndex: 'maquinas',
                    sortable: true
                }
                ],
                layout: 'fit',

                viewConfig:{
                    forceFit: true
                },
                sm:
                new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners:{

                        rowselect: function( selModel , index , data )
                        {
                            lineasGrid2.sm.suspendEvents();
                            lineasGrid2.sm.selectRow(index);

                            askLine=data.get('tcc');
                            maquinasStore.reload({
                                params:{
                                    linea: askLine,
                                    estado: askStatus
                                },
                                add:false
                            });
                        }
                    }
                }),

                width:640,
                height:200
            });

            maquinasGrid1= new Ext.grid.GridPanel( {
                store: maquinasStore,
                stripeRows : true,
                columns: [
                {
                    header: "ID",
                    width: 10,
                    dataIndex: 'id',
                    sortable: true
                },

                {
                    header: "Descripción",
                    width: 60,
                    dataIndex: 'descripcion',
                    sortable: true
                },

                {
                    header: "Descripción corta",
                    width: 60,
                    dataIndex: 'corta',
                    sortable: true
                },

                {
                    header: "Estado",
                    width: 20,
                    dataIndex: 'estado',
                    sortable: true
                },

                {
                    header: "Creación",
                    width: 70,
                    dataIndex: 'creacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Actualización",
                    width: 70,
                    dataIndex: 'actualizacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Partes",
                    width: 20,
                    dataIndex: 'partes',
                    sortable: true
                }
                ],
                tbar:new Ext.Toolbar([

                {
                    xtype: 'tbsplit',
                    text: 'ESTADO',

                    menu: new Ext.menu.Menu({
                        items: [
                        // These items will display in a dropdown menu when the split arrow is clicked
                        {
                            text: 'ACTIVA',
                            checked: true,
                            group: 'theme',
                            handler:function(){
                                askStatus='A';
                                maquinasStore.reload({
                                    params:{
                                        linea: askLine,
                                        estado:askStatus
                                    },
                                    add:false
                                });
                            }
                        },
                        {
                            text: 'INACTIVA',
                            checked: false,
                            group: 'theme',
                            handler:function(){
                                askStatus='I';
                                maquinasStore.reload({
                                    params:{
                                        linea: askLine,
                                        estado:askStatus
                                    },
                                    add:false
                                });
                            }
                        }
                        ]
                    })
                }

                ]),
                viewConfig: {
                    forceFit: true
                },
                sm: new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners:
                    {
                        rowselect   : function( selModel , index , data )
                        {
                            maquinasGrid2Sm.suspendEvents();
                            maquinasGrid2Sm.selectRow(index);

                            askMachine=data.get('id');
                            partesStore.reload({
                                params:{
                                    maquina: askMachine
                                },
                                add:false
                            });
                        }
                    }
                }),
                width:640,
                height:200
            });
        }

    };
}();


Ext.EventManager.onDocumentReady(TreeTest.init, TreeTest, true);