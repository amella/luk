var Tree = Ext.tree;
var emb=Ext.MessageBox;
var tree ;
var root;
var myMask ;
var deptosGrid;
var deptosStore;
var lineasGrid1;
var lineasGrid2;
var lineasGrid2Sm;
var lineasStore;
var maquinasGrid1;
var maquinasGrid2;
var maquinasGrid2Sm;
var maquinasStore;
var partesStore;
var searchStore;
var partesGrid;
var askDepartament=escape('%');
var askLine='w';
var espacio=' ';
var none='noneAll' ;
var askStatus='A';
var first=true;
var askMachine=null;
var newDeptoWindow;
var editDeptoWindow;
var newLineWindow;
var editLineWindow;
var newMachineWindow;
var editMachineWindow;
var newPartWindow;
var editPartWindow;
var deletePartWindow;
var searchPartWindow;
var deleteDeptoWindow;
var deleteLineaWindow;
var deleteMaquinaWindow;
var deletePartWindow;
var userTxt;

var depto_to_load=null;
var tcc_to_load=null;
var maq_to_load=null;
var part_to_load=null;

var usr=null;
var ct=null;
var ca=null;
var fff=null;
var iff=null;

function showDeleteDeptoWindow()
{
    var record=deptosGrid.sm.getSelected();
    if(!record) return;
    showDeleteDeptoPassWindow();
   
}

function showDeleteDeptoPassWindow()
{
     if(!deleteDeptoWindow)
    {
        deleteDeptoWindow = new Ext.Window({
            title: 'Autorización requerida',
            width: 350,
            height:150,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [deleteDeptoForm],
            closeAction :'hide'
        });
    }
    deleteDeptoForm.form.reset();
    deleteDeptoWindow.show();
}
function showDeleteLineaPassWindow()
{
     if(!deleteLineaWindow)
    {
        deleteLineaWindow = new Ext.Window({
            title: 'Autorización requerida',
            width: 350,
            height:150,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [deleteLineaForm],
            closeAction :'hide'
        });
    }
    deleteLineaForm.form.reset();
    deleteLineaWindow.show();
}
function showDeleteMaquinaPassWindow()
{
     if(!deleteMaquinaWindow)
    {
        deleteMaquinaWindow = new Ext.Window({
            title: 'Autorización requerida',
            width: 350,
            height:150,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [deleteMaquinaForm],
            closeAction :'hide'
        });
    }
    deleteMaquinaForm.form.reset();
    deleteMaquinaWindow.show();
}
function showDeletePartePassWindow()
{
     if(!deletePartWindow)
    {
        deletePartWindow = new Ext.Window({
            title: 'Autorización requerida',
            width: 400,
            height:150,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [deletePartForm],
            closeAction :'hide'
        });
    }
    deletePartForm.form.reset();
    deletePartWindow.show();
}
function showEditDeptoWindow()
{

    var record=deptosGrid.sm.getSelected();
    if(!record) return;
    editDeptoForm.form.reset();
  
    Ext.getCmp("edfId").setValue(record.get('id'));
    Ext.getCmp("edfDescripcion").setValue(record.get('descripcion'));
    Ext.getCmp("edfOldId").setValue(record.get('id'));

    if(!editDeptoWindow)
    {
        editDeptoWindow = new Ext.Window({
            title: 'Editar Departamento',
            width: 450,
            height:220,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [editDeptoForm],
            closeAction :'hide'
        });
    }

    editDeptoWindow.show();
}

function showNewDeptoWindow()
{
    if(!newDeptoWindow)
    {
        newDeptoWindow = new Ext.Window({
            title: 'Nuevo Departamento',
            width: 450,
            height:220,
            modal:true,     
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [newDeptoForm],
            closeAction :'hide'     
        });
    }
    newDeptoForm.form.reset();
    newDeptoWindow.show();
}

function showNewLineWindow()
{
    if(!newLineWindow)
    {
        newLineWindow = new Ext.Window({
            title: 'Nueva Linea',
            width: 450,
            height:220,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [newLineForm],
            closeAction :'hide'
        });
    }
    newLineForm.form.reset();
    var record=deptosGrid.sm.getSelected();
  
    Ext.getCmp("nlDepto").setValue(record.get('id'));


    newLineWindow.show();
}

function showEditLineWindow()
{
    var record=lineasGrid1.getSelectionModel().getSelected();
    if(!record) return;
    editLineForm.form.reset();

    Ext.getCmp("elfDesc").setValue(record.get('descripcion'));
   
    Ext.getCmp("elfId").setValue(record.get('tcc'));
    Ext.getCmp("nlfOldId").setValue(record.get('tcc'));

    if(!editLineWindow)
    {
        editLineWindow = new Ext.Window({
            title: 'Editar Linea',
            width: 450,
            height:220,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [editLineForm],
            closeAction :'hide'
        });
    }

    editLineWindow.show();
}

function showDeleteLineaWindow()
{
    var record=lineasGrid1.getSelectionModel().getSelected();
    if(!record) return;
    showDeleteLineaPassWindow();
 
}

function showDeleteMachineWindow()
{
    var record=maquinasGrid1.getSelectionModel().getSelected();
    if(!record) return;
    showDeleteMaquinaPassWindow();
  
}

function showNewMachineWindow()
{
    if(!newMachineWindow)
    {
        newMachineWindow = new Ext.Window({
            title: 'Nueva Maquina',
            width: 450,
            height:320,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [newMachineForm],
            closeAction :'hide'
        });
    }
    newMachineForm.form.reset();

    var record=lineasGrid2Sm.getSelected();

    Ext.getCmp("nmLinea").setValue(record.get('tcc'));


    newMachineWindow.show();
}

function showEditMachineWindow()
{
    var record=maquinasGrid1.getSelectionModel().getSelected()
    if(!record) return;
    editMachineForm.form.reset();
   
    Ext.getCmp('emfId').setValue(record.get('id'));
    Ext.getCmp('emfActiva').setValue(record.get('estado')=='ACTIVA');
    Ext.getCmp('emfDescripcion').setValue(record.get('descripcion'));
    Ext.getCmp('emfDescripcionCorta').setValue(record.get('corta'));
    Ext.getCmp('emfOldId').setValue(record.get('id'));
    if(!editMachineWindow)
    {
        editMachineWindow = new Ext.Window({
            title: 'Editar Maquina',
            width: 450,
            height:320,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [editMachineForm],
            closeAction :'hide'
        });
    }

    editMachineWindow.show();
}

function showNewPartWindow()
{
   
    if(!newPartWindow)
    {
        newPartWindow = new Ext.Window({
            title: 'Nueva Parte',
            width: 450,
            height:380,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [newPartForm],
            closeAction :'hide'
        });
    }
    var record=maquinasGrid1.getSelectionModel().getSelected()
    if(!record) return;
    newPartForm.form.reset();

    Ext.getCmp('npMaquina').setValue(record.get('id'));

    newPartWindow.show();    
}

function showEditPartWindow()
{
    if(!editPartWindow)
    {
        editPartWindow = new Ext.Window({
            title: 'Editar Parte',
            width: 450,
            height:380,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [editPartForm],
            closeAction :'hide'
        });
    }
    var mrecord=maquinasGrid1.getSelectionModel().getSelected();
    var record=partesGrid.sm.getSelected();
    if(!record) return;
    editPartForm.form.reset();
      Ext.getCmp( "epfcantidad").setValue(record.get('cantidad'));
    Ext.getCmp( "epfcaja").setValue(record.get('caja'));
    Ext.getCmp( "epfposicion").setValue(record.get('posicion'));
    Ext.getCmp("epffila").setValue(record.get('fila'));
    Ext.getCmp( "epfanaquel").setValue(record.get('anaquel'));
    Ext.getCmp("descripcion").setValue(record.get('descripcion'));
    Ext.getCmp( "descripcionCorta").setValue(record.get('corta'));
    Ext.getCmp('epMaquina').setValue(mrecord.get('id'));
    Ext.getCmp('epfoldcaja').setValue(record.get('caja'));
    Ext.getCmp('epfoldposicion').setValue(record.get('posicion'));


    editPartWindow.show();
}

function showDeletePartWindow()
{
    var record=partesGrid.sm.getSelected();

    if(!record) return;
    showDeletePartePassWindow();
   
}

function showSearchPartWindow()
{

    if(!searchPartWindow)
    {
        searchPartWindow = new Ext.Window({
            title: 'Buscar Parte',
            width: 902,
            height:390,
            modal:true,
            resizable:false,
            plain:false,
            hideParent:true,
            floating:true,
            bodyStyle:'padding:5px;',
            buttonAlign:'center',
            items: [searchPartForm],
            closeAction :'hide'
        });
    }
    searchStore.reload({params:{d:' ',c:' '},add:false});
    searchPartForm.form.reset();
    searchPartWindow.show();
}

function doHisto()
{

usr=Ext.getCmp('usuario').getValue();

ct=Ext.getCmp('tc').getValue();
ca=Ext.getCmp('ac').getValue();
var i_f= Ext.getCmp('fi').getValue();
var ff= Ext.getCmp('ff').getValue();

if((ct.length==0)||(ca.length==0))
return;
try{ 
fff=ff.format('d/m/Y');
iff=i_f.format('d/m/Y');
}catch(Ex){return;}
if(usr!='%')
  if((!usr.match(/\d{8}/)))
    return;
 // baseParams:{usuario:usr,tipo:ct,accion:ca,fechai:iff,fechaf:fff}
  histoStore.baseParams.usuario=usr;
  histoStore.baseParams.tipo=ct;
   histoStore.baseParams.accion=ca;
    histoStore.baseParams.fechai=iff;
    histoStore.baseParams.fechaf=fff;
histoStore.load({params:{start:0, limit:25},add:false});
}   


 function doParte()
{

var desc=Ext.getCmp('sfDesc').getValue();
var desccorta=Ext.getCmp('sfDescCorta').getValue();

    desc=desc.replace(/%/, '');
    desccorta=desccorta.replace(/%/, '');
    
if((desc.length==0)&&(desccorta.length==0))
    return;

searchStore.reload({params:{d:desc,c:desccorta},add:false});
}

var TreeTest = function()
{
    return { 
        init : function()
        {

            histoStore = new Ext.data.JsonStore
                        (
                            {
                                baseParams:{usuario:usr,tipo:ct,accion:ca,fechai:iff,fechaf:fff},
                                totalProperty: 'totalCount',
                                url: 'histoController.htm?action=obtener',
                                root:'histo',
                                fields:
                                        [
                                            {name:'item',type:'string'},
                                            {name:'clave',type:'string'},
                                            {name:'descripcion',type:'string'},
                                            {name:'usuario',type:'string'},
                                            {name:'accion',type:'string'},
                                            {name:'fecha',type:'string'}
                                        ]
                            }
                        );




searchStore= new Ext.data.JsonStore({
                url: 'parteController.htm?action=search',
                 root:'parte',
                fields: [
                {
                    name:'departamento',
                    type:'string'
                },
 {
                    name:'linea',
                    type:'string'
                },
                 {
                    name:'maquina',
                    type:'string'
                },
                {
                    name:'descripcion',
                    type:'string'
                },
 {
                    name:'corta',
                    type:'string'
                },
                {
                    name:'cantidad',
                    type:'int'
                },

                {
                    name:'caja',
                    type:'string'
                },

                {
                    name:'posicion',
                    type:'string'
                },

                {
                    name:'fila',
                    type:'string'
                },

                {
                    name:'anaquel',
                    type:'string'
                }
                ]
            });



            deptosStore = new Ext.data.JsonStore({
                url: 'departamentoController.htm?action=departamentosParaTabla',
                root:'departamento',
                fields: [   {
                    name:'id',
                    type:'string'
                },

                {
                    name:'descripcion',
                    type:'string'
                },

                {
                    name:'creacion',
                    type:'string'
                },

                {
                    name:'actualizacion',
                    type:'string'
                },

                {
                    name:'lineas',
                    type:'int'
                }
                ]
            });
                 
            deptosStore.on('load' ,function(store, records,  options)
            {
                if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;
                    askDepartament=none;
                    lineasStore.reload({params:{departamento: askDepartament},add:false});
                    return;
                }

                var indice=0;

                if(depto_to_load!=null)
                {                  
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('id')==depto_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }
                deptosGrid.sm.selectRow( (indice), true );
            });
 
            lineasStore = new Ext.data.JsonStore({

                url: 'lineaController.htm?action=lineasParaTabla',
                root:'linea',
                fields: [
                        {name:'tcc', type:'string'},

                {
                    name:'descripcion',
                    type:'string'
                },

                {
                    name:'creacion',
                    type:'string'
                },

                {
                    name:'actualizacion',
                    type:'string'
                },

                {
                    name:'maquinas',
                    type:'int'
                }
                ]
            });

            lineasStore.on('load' ,function(store, records,  options)
            {

               
                if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;
                    askLine=none;
                      maquinasStore.reload({
                            params:{
                                linea: askLine,
                                estado: 'J'
                            },
                            add:false
                        });

                    return;
                }

                var indice=0;

                if(tcc_to_load!=null)
                {
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('tcc')==tcc_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }

                lineasGrid2Sm.selectRow( indice, true );
               
            });

            maquinasStore = new Ext.data.JsonStore({

                url: 'maquinaController.htm?action=maquinasParaTabla',
                root:'maquina',
                fields: [
                {
                    name:'id',
                    type:'string'
                },

                {
                    name:'descripcion',
                    type:'string'
                },

                {
                    name:'corta',
                    type:'string'
                },

                {
                    name:'estado',
                    type:'string'
                },

                {
                    name:'creacion',
                    type:'string'
                },

                {
                    name:'actualizacion',
                    type:'string'
                },

                {
                    name:'partes',
                    type:'int'
                }
                ]
            });

            maquinasStore.on('load' ,function(store, records,  options)
            {

              
                if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;
                           
                    askMachine=none;
                    partesStore.reload({
                        params:{
                            maquina: askMachine
                        },
                        add:false
                    });
                    return;
                }
                  
                lineasGrid2.sm.resumeEvents();
                lineasGrid1.getSelectionModel().resumeEvents();
                 var indice=0;

                if(maq_to_load!=null)
                {
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('id')==maq_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }



                    maquinasGrid2Sm.selectRow( indice, true );
              
            });

            partesStore = new Ext.data.JsonStore({
                url: 'parteController.htm?action=partesParaTabla',
                root:'parte',
                fields: [
                {
                    name:'numero',
                    type:'string'
                },

                {
                    name:'descripcion',
                    type:'string'
                },
                {
                    name:'corta',
                    type:'string'
                },
                {
                    name:'cantidad',
                    type:'int'
                },

                {
                    name:'caja',
                    type:'string'
                },

                {
                    name:'posicion',
                    type:'string'
                },

                {
                    name:'fila',
                    type:'string'
                },

                {
                    name:'anaquel',
                    type:'string'
                },

                {
                    name:'creacion',
                    type:'string'
                },

                {
                    name:'actualizacion',
                    type:'string'
                }
                ]
            });

            partesStore.on('load' ,function(store, records,  options)
            {
            
               if(records.length==0)
                {
                    depto_to_load=null;
                    tcc_to_load=null;
                    maq_to_load=null;
                    part_to_load=null;
return;                    
                }

                maquinasGrid2Sm.resumeEvents();
                maquinasGrid1.getSelectionModel().resumeEvents();
                 
var indice=0;

                if(part_to_load!=null)
                {
                    for(var k=0;k<records.length;k++)
                    {
                        if(records[k].get('numero')==part_to_load)
                        {
                            indice=k;
                            break;
                        }
                    }

                }
  
                    partesGrid.sm.selectRow( indice, true );
                
                depto_to_load=null;
                tcc_to_load=null;
                maq_to_load=null;
                part_to_load=null;
            });


            lineasGrid2Sm=new Ext.grid.RowSelectionModel(
            {
                singleSelect: true,
                listeners:{
                    rowselect: function( selModel , index , data )
                    {
                        lineasGrid1.getSelectionModel().suspendEvents();
                        lineasGrid1.getSelectionModel().selectRow(index);
                        askLine=data.get('tcc');
                        maquinasStore.reload({
                            params:{
                                linea: askLine,
                                estado: askStatus
                            },
                            add:false
                        });
                    }
                }
            }
            );

            maquinasGrid2Sm=new Ext.grid.RowSelectionModel(
            {
                singleSelect: true,
                listeners:
                {
                    rowselect   : function( selModel , index , data )
                    {
                        
                        maquinasGrid1.getSelectionModel().suspendEvents();
                        maquinasGrid1.getSelectionModel().selectRow(index);
                        askMachine=data.get('id');
                        partesStore.reload({
                            params:{
                                maquina: askMachine
                            },
                            add:false
                        });
                    }
                }
            }
            );

            newDeptoForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,

                items: [              
                {
                    name : "ndfid",
                    width : 35,
                    xtype : "textfield",
                    regex:/\w{2}/,
                    regexText:"Clave de 2 caracteres.",
                    maxLength : "2",
                    maxLengthText : "Clave de 2 caracteres.",
                    minLengthText : "Clave de 2 caracteres.",
                    minLength : "2",
                    fieldLabel : "ID",
                    allowBlank: false
                },
                 {
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                }
                ]
            });
 
            newDeptoForm.addButton({
                text: 'Guardar',
                handler: function(){
                    newDeptoForm.getForm().submit({
                        url:'departamentoController.htm?action=nuevo',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            depto_to_load=newDeptoForm.getForm().findField('ndfid').getValue() ;
                            depto_to_load=depto_to_load.toUpperCase();
                            root.reload();
                            newDeptoWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

            editDeptoForm=new Ext.form.FormPanel({
    
                frame: true,
                title:'Datos',
                waitMsgTarget: true,

                items: [                
                {
                    id:'edfId',
                    name : "id",
                    width : 35,
                    xtype : "textfield",
                    regex:/\w{2}/,
                    regexText:"Clave de 2 caracteres.",
                    maxLength : "2",
                    maxLengthText : "Clave de 2 caracteres.",
                    minLengthText : "Clave de 2 caracteres.",
                    minLength : "2",
                    fieldLabel : "ID",
                    allowBlank: false
                },
                {
                    id:'edfDescripcion',
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                },{
                    id:'edfOldId',
                    xtype : "hidden",
                    name:'oldId'
                }
                ]
            });

            editDeptoForm.addButton({
                text: 'Guardar',
                handler: function(){
                    editDeptoForm.getForm().submit({
                        url:'departamentoController.htm?action=edit',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            depto_to_load=editDeptoForm.getForm().findField('edfId').getValue() ;
                            depto_to_load=depto_to_load.toUpperCase();
                            root.reload();
                            editDeptoWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

            newLineForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,

                items: [              
                { 
                    id:'tcc',
                    name : "tcc",
                    width : 80,
                    xtype : "textfield",
                    maskRe :/[0-9-]/,
                    regex:/0{2}5{1}2{1}-[0-9]{1,5}/,
                    regexText:"Formato: 0052-X[XXXX]",
                    maxLength : "10",
                    maxLengthText : "Formato: 0052-X[XXXX]",
                    minLengthText : "Formato: 0052-X[XXXX]",
                    minLength : "6",
                    fieldLabel : "TCC",
                    value:'0052-',
                    allowBlank: false
                },
                {
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                }
                ,
                {
                    id:'nlDepto',
                    xtype : "hidden",
                    name:'departamento'
                }
                ]
            });

            newLineForm.addButton({
                text: 'Guardar',
                handler: function(){
                    newLineForm.getForm().submit({
                        url:'lineaController.htm?action=nuevo',
                        waitMsg:'Guardando ...',
                        success: function( frm,act ) 
                        {                           
                            var depto_record=deptosGrid.sm.getSelected();
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=newLineForm.getForm().findField('tcc').getValue() ;
                            tcc_to_load=tcc_to_load.replace(/0052-/, '');                         
                            var limit=5-tcc_to_load.length;
                            var tmp="0052-";
                            for(var t=0;t<limit;t++)
                                tmp+="0";
                            tmp+=tcc_to_load;
                            tcc_to_load=tmp;
                            root.reload();
                            newLineWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

            editLineForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,

                items: [
               
                {
                    id:'elfId',
                    name : "tcc",
                    width : 80,
                    xtype : "textfield",
                    maskRe :/[0-9-]/,
                    regex:/0{2}5{1}2{1}-[0-9]{1,5}/,
                    regexText:"Formato: 0052-X[XXXX]",
                    maxLength : "10",
                    maxLengthText : "Formato: 0052-X[XXXX]",
                    minLengthText : "Formato: 0052-X[XXXX]",
                    minLength : "6",
                    fieldLabel : "TCC",
                    allowBlank: false
                },
                {
                    id:'elfDesc',
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                }
                ,
                {
                    id:'nlfOldId',
                    xtype : "hidden",
                    name:'oldTcc'
                }
                ]
            });

            editLineForm.addButton({
                text: 'Guardar',
                handler: function(){
                
                    editLineForm.getForm().submit({
                        url:'lineaController.htm?action=editar',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        { 
                            var depto_record=deptosGrid.sm.getSelected();
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=editLineForm.getForm().findField('elfId').getValue();
                            tcc_to_load=tcc_to_load.replace(/0052-/, '');
                            var limit=5-tcc_to_load.length;
                            var tmp="0052-";
                            for(var t=0;t<limit;t++)
                                tmp+="0";
                            tmp+=tcc_to_load;
                            tcc_to_load=tmp;
                            root.reload();
                            editLineWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }
                    });
                }
            });


            newMachineForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [               
                {
                    name : "nmid",
                    width : 70,
                    xtype : "textfield",
                    maskRe :/\d{1,8}/,
                    regex:/\d{1,8}/,
                    regexText:"Número de 8 digitos.",
                    maxLength : "8",
                    maxLengthText : "Número de 8 digitos.",
                    minLengthText : "Número de 8 digitos.",
                    minLength : "1",
                    fieldLabel : "ID",
                    allowBlank: false
                },
                {
                    xtype:'checkbox' ,
                    checked: true,
                    fieldLabel: 'Activa',
                        
                    name: 'activa'
                },
                {
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                }
                ,
                {
                    width : 300,
                    xtype : "textarea",
                    name: "descripcionCorta",
                    maxLength : "32",
                    maxLengthText : "Sólo se permiten 32 caracteres.",
                    fieldLabel : "Descripción corta"
                }
                ,
                {
                    id:'nmLinea',
                    xtype : "hidden",
                    name:'linea'
                }
                ]
            });



            newMachineForm.addButton({
                text: 'Guardar',
                handler: function(){
                    newMachineForm.getForm().submit({
                        url:'maquinaController.htm?action=nuevo',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            var depto_record=deptosGrid.sm.getSelected();
                            var line_record=lineasGrid2Sm.getSelected()
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=line_record.get('tcc');                            
                            maq_to_load=newMachineForm.getForm().findField('nmid').getValue();
                            root.reload();
                            newMachineWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

            editMachineForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,

                items: [
                
                {
                    id:'emfId',
                    name : "id",
                    width : 70,
                    xtype : "textfield",
                    maskRe :/\d{1,8}/,
                    regex:/\d{1,8}/,
                    regexText:"Número de 8 digitos.",
                    maxLength : "8",
                    maxLengthText : "Número de 8 digitos.",
                    minLengthText : "Número de 8 digitos.",
                    minLength : "1",
                    fieldLabel : "ID",
                    allowBlank: false
                },
                {
                    id:'emfActiva',
                    xtype:'checkbox' ,
                    fieldLabel: 'Activa',

                    name: 'activa'
                },
                {
                    id:'emfDescripcion',
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                }
                ,
                {
                    id:'emfDescripcionCorta',
                    width : 300,
                    xtype : "textarea",
                    name: "descripcionCorta",
                    maxLength : "32",
                    maxLengthText : "Sólo se permiten 32 caracteres.",
                    fieldLabel : "Descripción corta"
                },
                {
                    id:'emfOldId',
                    xtype : "hidden",
                    name:'oldId'
                }

                ]
            });

            editMachineForm.addButton({
                text: 'Guardar',
                handler: function(){
                    editMachineForm.getForm().submit({
                        url:'maquinaController.htm?action=edit',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            var depto_record=deptosGrid.sm.getSelected();
                            var line_record=lineasGrid2Sm.getSelected()
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=line_record.get('tcc');
                            maq_to_load=editMachineForm.getForm().findField('emfId').getValue();
                            root.reload();
                            editMachineWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });
            
            deleteDeptoForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {
                    name : "contrasenya",
                    width : 150,
                    xtype : "textfield",
                    inputType: 'password',
                    maxLength : "12",
                    maxLengthText : "Doce caracteres son requeridos.",
                    minLengthText : "Doce caracteres son requeridos.",
                    minLength : "12",
                    fieldLabel : "Contraseña",
                    allowBlank: false
                    }

                ]
            });

            deleteDeptoForm.addButton({
                text: 'Enviar',
                handler: function(){
                    deleteDeptoForm.getForm().submit({
                        url:'securityController.htm?action=check',
                        waitMsg:'Validando ...',
                        success: function( frm,act )
                        {
                             var record=deptosGrid.sm.getSelected();
                            new Ajax.Request('departamentoController.htm?action=delete&id='+record.get('id'),
            {
                method:'post',
                onSuccess:
                function(transport)
                {
                    var response = transport.responseText ;
                    if(response.length!=0)
                        Ext.Msg.alert( 'Error', response ) ;
                    else
                    {

                        depto_to_load=null;
                        tcc_to_load=null;
                        maq_to_load=null;
                        part_to_load=null;
                        deptosStore.reload();
                        root.reload();

                    }
                },
                onFailure:
                function(){
                    alert('Something went wrong...')
                }
            });
                            deleteDeptoWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });
deleteLineaForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {
                    name : "contrasenya",
                    width : 150,
                    xtype : "textfield",
                    inputType: 'password',
                    maxLength : "12",
                    maxLengthText : "Doce caracteres son requeridos.",
                    minLengthText : "Doce caracteres son requeridos.",
                    minLength : "12",
                    fieldLabel : "Contraseña",
                    allowBlank: false
                    }

                ]
            });

            deleteLineaForm.addButton({
                text: 'Enviar',
                handler: function(){
                    deleteLineaForm.getForm().submit({
                        url:'securityController.htm?action=check',
                        waitMsg:'Validando ...',
                        success: function( frm,act )
                        {
                             var record=lineasGrid1.getSelectionModel().getSelected();
                            new Ajax.Request('lineaController.htm?action=delete&tcc='+record.get('tcc'),
			                {
			                    method:'post',
			                    onSuccess:
			                    function(transport)
			                    {
			                        var response = transport.responseText ;
			                        if(response.length!=0)
			                            Ext.Msg.alert( 'Error', response ) ;
			                        else
			                        {

			                            tcc_to_load=null;
			                            maq_to_load=null;
			                            part_to_load=null;
			                            deptosStore.reload();
			                            root.reload();

			                        }
			                    },
			                    onFailure:
			                    function(){
			                        alert('Something went wrong...')
			                    }
            });
                            deleteLineaWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });


deleteMaquinaForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {
                    name : "contrasenya",
                    width : 150,
                    xtype : "textfield",
                    inputType: 'password',
                    maxLength : "12",
                    maxLengthText : "Doce caracteres son requeridos.",
                    minLengthText : "Doce caracteres son requeridos.",
                    minLength : "12",
                    fieldLabel : "Contraseña",
                    allowBlank: false
                    }

                ]
            });

            deleteMaquinaForm.addButton({
                text: 'Enviar',
                handler: function(){
                    deleteMaquinaForm.getForm().submit({
                        url:'securityController.htm?action=check',
                        waitMsg:'Validando ...',
                        success: function( frm,act )
                        {

                       var record=maquinasGrid1.getSelectionModel().getSelected();
                      new Ajax.Request('maquinaController.htm?action=delete&id='+record.get('id'),
		                  {
		                      method:'post',
		                      onSuccess:
		                      function(transport)
		                      {
		                          var response = transport.responseText ;
		                          if(response.length!=0)
		                              Ext.Msg.alert( 'Error', response ) ;
		                          else
		                          {

		                              maq_to_load=null;
		                              part_to_load=null;
		                              lineasStore.reload();
		                              root.reload();
		                          }
		                      },
		                      onFailure:
		                      function(){
		                          alert('Something went wrong...')
		                      }
            });
                            deleteMaquinaWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

 deletePartForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {
                    name : "contrasenya",
                    width : 150,
                    xtype : "textfield",
                    inputType: 'password',
                    maxLength : "12",
                    maxLengthText : "Doce caracteres son requeridos.",
                    minLengthText : "Doce caracteres son requeridos.",
                    minLength : "12",
                    fieldLabel : "Contraseña",
                    allowBlank: false
                    }

                ]
            });

            deletePartForm.addButton({
                text: 'Enviar',
                handler: function(){
                    deletePartForm.getForm().submit({
                        url:'securityController.htm?action=check',
                        waitMsg:'Validando ...',
                        success: function( frm,act )
                        {
                       var record=partesGrid.sm.getSelected();
                  new Ajax.Request('parteController.htm?action=delete&numero='+record.get('numero'),
		              {
		                  method:'post',
		                  onSuccess:
		                  function(transport)
		                  {
		                      var response = transport.responseText ;
		                      if(response.length!=0)
		                          Ext.Msg.alert( 'Error', response ) ;
		                      else
		                      {

		                          depto_to_load=null;
		                          tcc_to_load=null;
		                          maq_to_load=null;
		                          part_to_load=null;
		                          deptosStore.reload();
		                          root.reload();

		                      }
		                  },
		                  onFailure:
		                  function(){
		                      alert('Something went wrong...')
		                  }
            });
                            deletePartWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

            newPartForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {
                    name : "npfcantidad",
                    width : 60,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1,5}/,
                    regexText:"Número de X[XXXX] digitos.",
                    maxLength : "5",
                    maxLengthText : "Número de X[XXXX] digitos.",
                    minLengthText : "Número de X[XXXX] digitos.",
                    minLength : "1",
                    fieldLabel : "Cantidad",
                    allowBlank: false
                },
                {
                    name : "npfcaja",
                    width : 20,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1}/,
                    regexText:"Número de 1 digitos.",
                    maxLength : "1",
                    maxLengthText : "Número de 1 digito.",
                    minLengthText : "Número de 1 digito.",
                    minLength : "1",
                    fieldLabel : "Caja",
                    allowBlank: false
                },
                {
                    name : "npfposicion",
                    width : 30,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{2}/,
                    regexText:"Número de 2 digitos.",
                    maxLength : "2",
                    maxLengthText : "Número de 2 digito.",
                    minLengthText : "Número de 2 digito.",
                    minLength : "2",
                    fieldLabel : "Posición",
                    allowBlank: false
                },
                {
                    name : "npffila",
                    width : 20,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1}/,
                    regexText:"Número de 1 digitos.",
                    maxLength : "1",
                    maxLengthText : "Número de 1 digito.",
                    minLengthText : "Número de 1 digito.",
                    minLength : "1",
                    fieldLabel : "Fila"
                    
                },
                {
                    name : "npfanaquel",
                    width : 30,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{2}/,
                    regexText:"Número de 2 digitos.",
                    maxLength : "2",
                    maxLengthText : "Número de 2 digito.",
                    minLengthText : "Número de 2 digito.",
                    minLength : "1",
                    fieldLabel : "Anaquel"
                   
                },
                {
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                }
                ,
                {
                    width : 300,
                    xtype : "textarea",
                    name: "descripcionCorta",
                    maxLength : "32",
                    maxLengthText : "Sólo se permiten 32 caracteres.",
                    fieldLabel : "Descripción corta"
                }
                ,
                {
                    id:'npMaquina',
                    xtype : "hidden",
                    name:'maquina'
                }
                ]
            });

            newPartForm.addButton({
                text: 'Guardar',
                handler: function(){
                    newPartForm.getForm().submit({
                        url:'parteController.htm?action=nuevo',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            var depto_record=deptosGrid.sm.getSelected();
                            var line_record=lineasGrid2Sm.getSelected();
                            var maq_record=maquinasGrid2Sm.getSelected();
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=line_record.get('tcc');
                            maq_to_load=maq_record.get('id');
                            part_to_load=maq_to_load+newPartForm.getForm().findField('npfcaja').getValue()+newPartForm.getForm().findField('npfposicion').getValue();
                           
                            root.reload();
                            
                            newPartWindow.hide();
                        },
                        failure: function( frm,act )
                        {
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

            editPartForm=new Ext.FormPanel({
                frame: true,
                title:'Datos',
                waitMsgTarget: true,
                items: [
                    {

                    id:'epfcantidad',
                    name : "epfcantidad",
                    width : 60,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1,5}/,
                    regexText:"Número de X[XXXX] digitos.",
                    maxLength : "5",
                    maxLengthText : "Número de X[XXXX] digitos.",
                    minLengthText : "Número de X[XXXX] digitos.",
                    minLength : "1",
                    fieldLabel : "Cantidad",
                    allowBlank: false
                },
                {
                    id: "epfcaja",
                    name : "epfcaja",
                    width : 20,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1}/,
                    regexText:"Número de 1 digitos.",
                    maxLength : "1",
                    maxLengthText : "Número de 1 digito.",
                    minLengthText : "Número de 1 digito.",
                    minLength : "1",
                    fieldLabel : "Caja",
                    allowBlank: false
                },
                {
                    id: "epfposicion",
                    name : "epfposicion",
                    width : 30,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{2}/,
                    regexText:"Número de 2 digitos.",
                    maxLength : "2",
                    maxLengthText : "Número de 2 digito.",
                    minLengthText : "Número de 2 digito.",
                    minLength : "2",
                    fieldLabel : "Posición",
                    allowBlank: false
                },
                {
                    id : "epffila",
                    name : "epffila",
                    width : 20,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{1}/,
                    regexText:"Número de 1 digitos.",
                    maxLength : "1",
                    maxLengthText : "Número de 1 digito.",
                    minLengthText : "Número de 1 digito.",
                    minLength : "1",
                    fieldLabel : "Fila",
                    allowBlank: false
                },
                {
                    id: "epfanaquel", 
                    name : "epfanaquel",
                    width : 30,
                    xtype : "textfield",
                    maskRe :/\d/,
                    regex:/\d{2}/,
                    regexText:"Número de 2 digitos.",
                    maxLength : "2",
                    maxLengthText : "Número de 2 digito.",
                    minLengthText : "Número de 2 digito.",
                    minLength : "1",
                    fieldLabel : "Anaquel",
                   allowBlank: false
                },
                {
                    id:"descripcion",
                    width : 300,
                    xtype : "textarea",
                    name: "descripcion",
                    maxLength : "128",
                    maxLengthText : "Sólo se permiten 128 caracteres.",
                    fieldLabel : "Descripción"
                }
                ,
                {
                    id: "descripcionCorta",
                    width : 300,
                    xtype : "textarea",
                    name: "descripcionCorta",
                    maxLength : "32",
                    maxLengthText : "Sólo se permiten 32 caracteres.",
                    fieldLabel : "Descripción corta"
                }
                ,
                {
                    id:'epMaquina',
                    xtype : "hidden",
                    name:'maquina'
                },
                {
                    id:'epfoldcaja',
                    xtype : "hidden",
                    name:'epfoldcaja'
                },
                {
                    id:'epfoldposicion',
                    xtype : "hidden",
                    name:'epfoldposicion'
                }
                ]
            });

            editPartForm.addButton({
                text: 'Guardar',
                handler: function(){
                    editPartForm.getForm().submit({
                        url:'parteController.htm?action=edit',
                        waitMsg:'Guardando ...',
                        success: function( frm,act )
                        {
                            var depto_record=deptosGrid.sm.getSelected();
                            var line_record=lineasGrid2Sm.getSelected();
                            var maq_record=maquinasGrid2Sm.getSelected();
                            depto_to_load=depto_record.get('id');
                            tcc_to_load=line_record.get('tcc');
                            maq_to_load=maq_record.get('id');
                            part_to_load=maq_to_load+editPartForm.getForm().findField('epfcaja').getValue()+editPartForm.getForm().findField('epfposicion').getValue();
                           
                            root.reload();
                              
                            editPartWindow.hide();
                        },
                        failure: function( frm,act )
                        {     
                            if(act.result)
                                Ext.Msg.alert( 'Error', act.result.message ) ;
                        }

                    });
                }
            });

           searchPartForm=new Ext.FormPanel(
           {
                                frame: true,
                                labelAlign: 'left',
                                title: 'Información',
                                bodyStyle:'padding:5px',
                                width: 875,
                                layout: 'column',
                                items: [{
                                        columnWidth: 0.7,
                                        layout: 'fit',
                                        items: {

                                            xtype: 'grid',
                                            layout: 'fit',
                                           store: searchStore,
                                           loadMask : true,
                                            stripeRows : true,
                                            viewConfig: {
                                                forceFit: true
                                            },
                                            columns: [
                                                {header: "Departamento",width: 20,dataIndex: 'departamento',sortable: true },
                                                {header: "Linea",width: 20,dataIndex: 'linea',sortable: true},
                                                {header: "Máquina",width: 90,dataIndex: 'maquina',sortable: true},
                                                {header: "Descripción",width: 90,dataIndex: 'descripcion',sortable: true},
                                                {header: "Descripcion Corta",width: 90,dataIndex: 'corta',sortable: true},
                                                {header: "Caja",width: 20,dataIndex: 'caja',sortable: true},
                                                {header: "Posicion",width: 20,dataIndex: 'posicion',sortable: true},
                                                {header: "Fila",width: 20,dataIndex: 'fila',sortable: true},
                                                {header: "Anaquel",width: 20,dataIndex: 'anaquel',sortable: true}
                                            ],
                                            sm: new Ext.grid.RowSelectionModel({
                                                singleSelect: true,
                                                listeners:
                                                    {

                                                    rowselect: function( selModel , index , data )
                                                    {

                                                    }
                                                }
                                            }),
                                            width:800,
                                            height:300


                                        }
                                    },{

                                        columnWidth: 0.3,
                                        xtype: 'fieldset',
                                        labelWidth: 90,
                                        title:'Filtro',
                                        defaults: {width: 120},	// Default config options for child items
                                        //defaultType: 'textfield',
                                        autoHeight: true,
                                        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                                        border: false,
                                        style: {
                                            "margin-left": "10px", // when you add custom margin in IE 6...
                                            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
                                        },
                                        items: [
                                            {
                                                id:'sfDesc',
                                                xtype:'textfield',                                                
                                                fieldLabel: 'Descripción',
                                                name: 'sfDesc'
                                               
                                            },
                                             {
                                                id:'sfDescCorta',
                                                xtype:'textfield',                                                
                                                fieldLabel: 'Descripción corta',
                                                name: 'sfDescCorta'
                                               
                                            },
                                            {xtype:'button',text:'Consultar',handler:doParte}]
                                    }]

                            }
            );

            Ext.QuickTips.init();

            tree= new Tree.TreePanel({
                el:'tree',
                animate:true, 
                autoScroll:true,                                         
                animCollapse : true,               
                trackMouseOver : true
            });
            tree.on('dblclick',function(node, e)
            {
                        
                var nId=node.id;
                if(nId=='main') return;
                if(nId.length==2)
                {
                    depto_to_load=nId;
                    deptosStore.reload({add:false});
                }
                else
                if(nId.length==8)
                {                   
                    maq_to_load=nId;
                    tcc_to_load=node.parentNode.id;
                    depto_to_load=node.parentNode.parentNode.id;
                     deptosStore.reload({add:false});
                }
                else
                if(nId.length==11)
                {
                   
                    part_to_load=nId;
                    maq_to_load=node.parentNode.id;
                    tcc_to_load=node.parentNode.parentNode.id;
                    depto_to_load=node.parentNode.parentNode.parentNode.id; 
                    deptosStore.reload({add:false});
                }
                else
                {                    
                    tcc_to_load=nId;
                    depto_to_load=node.parentNode.id;
                    deptosStore.reload({add:false});
                }
                        
            });

            tree.on('load',function()
            {
                if(first)
                {
                    deptosGrid.store.reload({
                        add:false
                    });
                    first=false;
                }
            }
            );

            root= new Tree.AsyncTreeNode({
                id:'main',
                text:'Departamentos',
                leaf:false,
                loader:new Tree.TreeLoader({
                    dataUrl:'arbol.htm'
                })
                });
            root.on('load',function()
            {
                if(depto_to_load!=null)
                {
                    deptosGrid.store.reload({
                        add:false
                    });
                }
                                
            });
            
            tree.setRootNode(root);                      
            tree.render();
            tree.syncSize();
            root.expand(false,false);

            lineasGrid1= new Ext.grid.GridPanel(
            {
                store: lineasStore,
                stripeRows : true,
                loadMask:false,
                columns: [
                {
                    header: "TCC",
                    width: 15,
                    dataIndex: 'tcc',
                    sortable: true
                },

                {
                    header: "Descripción",
                    width: 180,
                    dataIndex: 'descripcion',
                    sortable: true
                },

                {
                    header: "Creación",
                    width: 70,
                    dataIndex: 'creacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Actualización",
                    width: 70,
                    dataIndex: 'actualizacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Maquinas",
                    width: 115,
                    dataIndex: 'maquinas',
                    sortable: true
                }
                ], 
                layout: 'fit',
                tbar:new Ext.Toolbar([
                {
                    icon: 'resources/images/my/add.png',
                    cls: 'x-btn-icon',
                    tooltip : "Añadir",
                    handler:showNewLineWindow
                },

                {
                    xtype: 'tbseparator'
                },

                {
                    icon: 'resources/images/my/delete.png',
                    cls: 'x-btn-icon',
                    toltip:"Eliminar",
                    handler:showDeleteLineaWindow
                },

                {
                    xtype: 'tbseparator'
                },

                {
                    icon: 'resources/images/my/pencil.png',
                    cls: 'x-btn-icon',
                    tooltip : "Editar",
                    handler:showEditLineWindow
                }
                ]),
                viewConfig:{
                    forceFit: true
                },
                sm:
                new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners:{
                      
                        rowselect: function( selModel , index , data )
                        {                         
                            lineasGrid2.sm.suspendEvents();                         
                            lineasGrid2.sm.selectRow(index);
                            
                            askLine=data.get('tcc');
                            maquinasStore.reload({
                                params:{
                                    linea: askLine,
                                    estado: askStatus
                                },
                                add:false
                            });
                        }
                    }
                }),
   
                width:640,
                height:200               
            });

            maquinasGrid1= new Ext.grid.GridPanel( {
                store: maquinasStore,
                stripeRows : true,
                columns: [
                {
                    header: "ID",
                    width: 10,
                    dataIndex: 'id',
                    sortable: true
                },

                {
                    header: "Descripción",
                    width: 60,
                    dataIndex: 'descripcion',
                    sortable: true
                },

                {
                    header: "Descripción corta",
                    width: 60,
                    dataIndex: 'corta',
                    sortable: true
                },

                {
                    header: "Estado",
                    width: 20,
                    dataIndex: 'estado',
                    sortable: true
                },

                {
                    header: "Creación",
                    width: 70,
                    dataIndex: 'creacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Actualización",
                    width: 70,
                    dataIndex: 'actualizacion',
                    sortable: true,
                    align:'left'
                },

                {
                    header: "Partes",
                    width: 20,
                    dataIndex: 'partes',
                    sortable: true
                }
                ],
                tbar:new Ext.Toolbar([
                {
                    icon: 'resources/images/my/add.png',
                    cls: 'x-btn-icon',
                    tooltip : "Añadir",
                    handler:showNewMachineWindow
                },

                {
                    xtype: 'tbseparator'
                },

                {
                    icon: 'resources/images/my/delete.png',
                    cls: 'x-btn-icon',
                    tooltip : "Eliminar",
                    handler:showDeleteMachineWindow
                },

                {
                    xtype: 'tbseparator'
                },

                {
                    icon: 'resources/images/my/pencil.png',
                    cls: 'x-btn-icon',
                    tooltip : "Editar",
                    handler:showEditMachineWindow
                },

                {
                    xtype: 'tbsplit',
                    text: 'ESTADO',

                    menu: new Ext.menu.Menu({
                        items: [
                        // These items will display in a dropdown menu when the split arrow is clicked
                        {
                            text: 'ACTIVA',
                            checked: true,
                            group: 'theme', 
                            handler:function(){
                                askStatus='A';
                                maquinasStore.reload({
                                    params:{
                                        linea: askLine,
                                        estado:askStatus
                                    },
                                    add:false
                                });
                            }
                        },
                        {
                            text: 'INACTIVA',
                            checked: false,
                            group: 'theme', 
                            handler:function(){
                                askStatus='I';
                                maquinasStore.reload({
                                    params:{
                                        linea: askLine,
                                        estado:askStatus
                                    },
                                    add:false
                                });
                            }
                        }
                        ]
                    })
                }

                ]),
                viewConfig: {
                    forceFit: true
                },
                sm: new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners:
                    {
                        rowselect   : function( selModel , index , data ) 
                        {
                            maquinasGrid2Sm.suspendEvents();
                            maquinasGrid2Sm.selectRow(index);
                          
                            askMachine=data.get('id');
                            partesStore.reload({
                                params:{
                                    maquina: askMachine
                                },
                                add:false
                            });
                        }
                    }
                }),
                width:640,
                height:200                
            });         
        }

    };
}();


Ext.EventManager.onDocumentReady(TreeTest.init, TreeTest, true);