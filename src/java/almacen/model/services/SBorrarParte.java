package almacen.model.services;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/*
 *  @author Andrés M. Romero
 */
public class SBorrarParte extends StoredProcedure
{
 public static final String PROC_NAME = "sp_eliminar_parte";

 public SBorrarParte(DataSource ds)
  {
   setDataSource(ds);
   setSql(PROC_NAME);

   declareParameter(new SqlParameter("numero", Types.VARCHAR));
     declareParameter(new SqlParameter("usuario", Types.INTEGER));
   declareParameter(new SqlOutParameter( "mensaje",Types.VARCHAR ));
   compile();
  }

  public Map doCall(String numero,String usuario)
  {
   Map inParameters = new HashMap();
   inParameters.put( "numero",numero);
   inParameters.put( "usuario",usuario);
   Map out = execute( inParameters );
   return out;
  }
}
