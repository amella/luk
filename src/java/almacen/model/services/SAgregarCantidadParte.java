package almacen.model.services;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/*
 *  @author Andrés M. Romero
 */
public class SAgregarCantidadParte extends StoredProcedure
{
 public static final String PROC_NAME = "sp_agregar_cantidad";

 public SAgregarCantidadParte(DataSource ds)
  {
   setDataSource(ds);
   setSql(PROC_NAME);

   declareParameter(new SqlParameter("caja", Types.INTEGER));
   declareParameter(new SqlParameter("posicion", Types.INTEGER));

   declareParameter(new SqlParameter("maquina", Types.INTEGER));
   declareParameter(new SqlParameter("cantidad", Types.INTEGER));
    declareParameter(new SqlParameter("usuario", Types.INTEGER));
   declareParameter(new SqlOutParameter( "mensaje",Types.VARCHAR ));
   compile();
  }

  public Map doCall(String caja, String posicion,String maquina,String cantidad,String usuario)
  {
   Map inParameters = new HashMap();
   inParameters.put( "caja",caja);
   inParameters.put( "posicion",posicion);

   inParameters.put( "maquina",maquina);

   inParameters.put( "cantidad",cantidad);
    inParameters.put( "usuario",usuario);
   Map out = execute( inParameters );
   return out;
  }
}

