package almacen.model.services;

import almacen.model.classes.CLinea;
import almacen.model.classes.CLineaTree;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import almacen.model.interfaces.ILinea;
import java.util.Map;
/*
 *  @author Andrés M. Romero
 */
public class SLinea extends JdbcDaoSupport implements ILinea
{

    public List<CLinea> obtenerLineas(String departamento) {
        final ArrayList<CLinea> results = new ArrayList<CLinea>();
        JdbcTemplate template = getJdbcTemplate();

        String QUERY = "select t.tcc, t.descripcion, t.fecha_c, t.fecha_a, t.num_maquinas from fv_almacen_linea t where t.departamento like '" + departamento + "' order by t.tcc";
      
        template.query(QUERY,
                new RowCallbackHandler() {

                    public void processRow(ResultSet rs) throws SQLException {
                        CLinea linea = new CLinea();
                        linea.setId(rs.getString(1));
                        linea.setDescripcion(rs.getString(2));
                        linea.setFechaCreacion(rs.getString(3));
                        linea.setFechaActualizacion(rs.getString(4));
                        linea.setTieneHijos(rs.getString(5));
                        results.add(linea);
                    }
                });
        return results;
    }

    public List<CLineaTree> obtenerLineasArbol(String departamento) {
        final ArrayList<CLineaTree> results = new ArrayList<CLineaTree>();
        JdbcTemplate template = getJdbcTemplate();

        String QUERY = "select t.tcc, t.descripcion, t.num_maquinas from fv_almacen_linea t where t.departamento like '" + departamento + "' order by t.tcc";

        template.query(QUERY,
                new RowCallbackHandler() {

                    public void processRow(ResultSet rs) throws SQLException {
                        CLineaTree linea = new CLineaTree();
                        linea.setId(rs.getString(1));
                        linea.setDescripcion(rs.getString(2));
                        linea.setTieneHijos(rs.getString(3));
                        results.add(linea);
                    }
                });
        return results;
    }

    public int contar() {
        JdbcTemplate template = getJdbcTemplate();
        String query = "select count(t.id_linea) from fv_almacen_linea t";
        return template.queryForInt(query);
    }

    public String nuevaLinea(String tcc, String descripcion, String departamento,String usuario) {
        SNuevaLinea snd = new SNuevaLinea(this.getDataSource());
        Map result = snd.doCall(tcc, descripcion, departamento,usuario);
        return (String) (result.get("mensaje"));
    }

    public String editarLinea(String id, String id_old, String descripcion,String usuario) {
        SEditarLinea snd = new SEditarLinea(this.getDataSource());
        Map result = snd.doCall(id, id_old, descripcion,usuario);
        return (String) (result.get("mensaje"));
    }

    public String borrarLinea(String id,String usuario) {
        SBorrarLinea sbd = new SBorrarLinea(this.getDataSource());
        Map result = sbd.doCall(id,usuario);
        return (String) (result.get("mensaje"));

    }
}

