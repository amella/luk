package almacen.model.services;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/*
 *  @author Andrés M. Romero
 */
public class SEditarDepartamento extends StoredProcedure
{
 public static final String PROC_NAME = "sp_actualiza_departamento";

 public SEditarDepartamento(DataSource ds)
  {
   setDataSource(ds);
   setSql(PROC_NAME);

   declareParameter(new SqlParameter("id", Types.VARCHAR));
   declareParameter(new SqlParameter("id_old", Types.VARCHAR));
   declareParameter(new SqlParameter("descripcion", Types.VARCHAR));
    declareParameter(new SqlParameter("usuario", Types.INTEGER));
   declareParameter(new SqlOutParameter( "mensaje",Types.VARCHAR ));
   compile();
  }

  public Map doCall(String id,String id_old, String descripcion,String usuario)
  {
   Map inParameters = new HashMap();
   inParameters.put( "id",id);
   inParameters.put( "id_old",id_old);
   inParameters.put( "descripcion",descripcion);
    inParameters.put( "usuario",usuario);
   Map out = execute( inParameters );
   return out;
  }
}
