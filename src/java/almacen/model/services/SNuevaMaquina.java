package almacen.model.services;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/*
 *  @author Andrés M. Romero
 */
public class SNuevaMaquina extends StoredProcedure
{
 public static final String PROC_NAME = "sp_nueva_maquina";

 public SNuevaMaquina(DataSource ds)
  {
   setDataSource(ds);
   setSql(PROC_NAME);

   declareParameter(new SqlParameter("id", Types.INTEGER));
   declareParameter(new SqlParameter("descripcion", Types.VARCHAR));
    declareParameter(new SqlParameter("descripcion_corta", Types.VARCHAR));
     declareParameter(new SqlParameter("estado", Types.VARCHAR));  
   declareParameter(new SqlParameter("linea",Types.VARCHAR));
     declareParameter(new SqlParameter("usuario", Types.INTEGER));
   declareParameter(new SqlOutParameter( "mensaje",Types.VARCHAR ));
   compile();
  }

  public Map doCall(String id, String descripcion, String descripcionCorta, String estado, String linea,String usuario)
  {
   Map inParameters = new HashMap();
   inParameters.put( "id",id);
   inParameters.put( "descripcion",descripcion);
   inParameters.put( "descripcion_corta",descripcionCorta);
   inParameters.put( "estado",estado);
   
   inParameters.put( "linea",linea);
     inParameters.put( "usuario",usuario);
   Map out = execute( inParameters );
   return out;
  }
}
