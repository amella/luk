package almacen.model.services;

import almacen.model.classes.CHistorico;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
/*
 *  @author Andrés M. Romero
 */

public class SHisto extends JdbcDaoSupport {

    private final static int PAGES = 25;

    public int contar(String tipo, String usuario, String accion, String fechaI, String fechaF) {
        JdbcTemplate template = getJdbcTemplate();

        String QUERY = "select count(t.item) " +
                "from fv_histo_all t " +
                "where t.item like '" + tipo + "' and t.usuario like '" + usuario + "' and t.accion like '" + accion + "' and trunc(t.fecha) between to_date('" + fechaI + "','dd/MM/yyyy') and to_date('" + fechaF + "','dd/MM/yyyy') " +
                " order by t.fecha";
        return template.queryForInt(QUERY);
    }

    public List<CHistorico> obtenerHistorico(String tipo, String usuario, String accion, String fechaI, String fechaF, String start, String limit) {
        final ArrayList<CHistorico> results = new ArrayList<CHistorico>();
        JdbcTemplate template = getJdbcTemplate();

        int iStart = Integer.parseInt(start);       
        int v_start = iStart + 1;
        int v_limit = iStart + PAGES;

        String QUERY = "select item, clave,descripcion, usuario,accion,to_char(fecha,'dd/MM/yyyy hh24:mi') " +
                "from (select a.* , rownum rnum from  fv_histo_all a where ROWNUM <= " + v_limit + " and  a.item like '" + tipo + "' and a.usuario like '" + usuario + "' and a.accion like '" + accion + "' and trunc(a.fecha) between to_date('" + fechaI + "','dd/MM/yyyy') and to_date('" + fechaF + "','dd/MM/yyyy') order by a.item,a.accion,a.fecha) " +
                "where rnum  >= " + v_start;

        template.query(QUERY,
                new RowCallbackHandler() {

                    public void processRow(ResultSet rs) throws SQLException {
                        CHistorico ch = new CHistorico();
                        ch.setItem(rs.getString(1));
                        ch.setClave(rs.getString(2));
                        ch.setDescripcion(rs.getString(3));
                        ch.setUsuario(rs.getString(4));
                        ch.setAccion(rs.getString(5));
                        ch.setFecha(rs.getString(6));
                        results.add(ch);
                    }
                });
        return results;
    }
}
