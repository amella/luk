package almacen.model.services;

import almacen.model.classes.CDepartamento;
import almacen.model.classes.CDepartamentoTree;
import almacen.model.interfaces.IDepartamento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
/*
 *  @author Andrés M. Romero
 */
public class SDepartamento extends JdbcDaoSupport implements IDepartamento {

    private final static String QUERY_DEPARTAMENTOS_ARLBOL = "select t.id_departamento,t.descripcion from fv_almacen_departamento t where t.num_lineas >0 order by t.num_lineas desc";
    private final static String QUERY_DEPARTAMENTOS = "select t.id_departamento,t.descripcion,t.fecha_c,t.fecha_a,t.num_lineas from fv_almacen_departamento t  order by t.num_lineas desc";

    public List<CDepartamento> obtenerDepartamentos() {
        final ArrayList<CDepartamento> results = new ArrayList<CDepartamento>();
        JdbcTemplate template = getJdbcTemplate();

        template.query(QUERY_DEPARTAMENTOS,
                new RowCallbackHandler() {

                    public void processRow(ResultSet rs) throws SQLException {
                        CDepartamento departamento = new CDepartamento();
                        departamento.setId(rs.getString(1));
                        departamento.setDescripcion(rs.getString(2));
                        departamento.setFechaCreacion(rs.getString(3));
                        departamento.setFechaActualizacion(rs.getString(4));
                        departamento.setTieneHijos(rs.getString(5));
                        results.add(departamento);
                    }
                });
        return results;
    }

    public List<CDepartamentoTree> obtenerDepartamentosArbol() {
        final ArrayList<CDepartamentoTree> results = new ArrayList<CDepartamentoTree>();
        JdbcTemplate template = getJdbcTemplate();

        template.query(QUERY_DEPARTAMENTOS_ARLBOL,
                new RowCallbackHandler() {

                    public void processRow(ResultSet rs) throws SQLException {
                        CDepartamentoTree departamento = new CDepartamentoTree();
                        departamento.setId(rs.getString(1));
                        departamento.setDescripcion(rs.getString(2));
                        results.add(departamento);
                    }
                });
        return results;
    }

    public int contarDepartamentosArbol() {
        JdbcTemplate template = getJdbcTemplate();
        String query = "select count(d.id_departamento) from fv_almacen_departamento  d num_lineas >0";
        return template.queryForInt(query);
    }

    public int contarDepartamentos() {
        JdbcTemplate template = getJdbcTemplate();
        String query = "select count(d.id_departamento) from fv_almacen_departamento  d ";
        return template.queryForInt(query);
    }

    public String nuevoDepartamento(String id, String descripcion,String usuario) {
        SNuevoDepartamento snd = new SNuevoDepartamento(this.getDataSource());
        Map result = snd.doCall(id, descripcion,usuario);
        return (String) (result.get("mensaje"));

    }

    public String editarDepartamento(String id, String id_old, String descripcion,String usuario) {
        SEditarDepartamento sed = new SEditarDepartamento(this.getDataSource());
        Map result = sed.doCall(id, id_old, descripcion,usuario);
        return (String) (result.get("mensaje"));
    }

    public String borrarDepartamento(String id,String usuario) {
        SBorrarDepartamento sbd = new SBorrarDepartamento(this.getDataSource());
        Map result = sbd.doCall(id,usuario);
        return (String) (result.get("mensaje"));

    }
}
