package almacen.model.services;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/*
 *  @author Andrés M. Romero
 */
public class SNuevaParte extends StoredProcedure
{
 public static final String PROC_NAME = "sp_nueva_parte";

 public SNuevaParte(DataSource ds)
  {
   setDataSource(ds);
   setSql(PROC_NAME);

   declareParameter(new SqlParameter("caja", Types.INTEGER));
   declareParameter(new SqlParameter("posicion", Types.INTEGER));
   declareParameter(new SqlParameter("fila", Types.INTEGER));
   declareParameter(new SqlParameter("anaquel", Types.INTEGER));
   declareParameter(new SqlParameter("descripcion", Types.VARCHAR));
   declareParameter(new SqlParameter("descripcion_corta", Types.VARCHAR));
   declareParameter(new SqlParameter("maquina", Types.INTEGER));
   declareParameter(new SqlParameter("cantidad", Types.INTEGER));
     declareParameter(new SqlParameter("usuario", Types.INTEGER));
   declareParameter(new SqlOutParameter( "mensaje",Types.VARCHAR ));
   compile();
  }

  public Map doCall(String caja, String posicion, String fila, String anaquel, String descripcion, String descripcionCorta, String maquina,String cantidad,String usuario)
  {
   Map inParameters = new HashMap();
   inParameters.put( "caja",caja);
   inParameters.put( "posicion",posicion);
   inParameters.put( "fila",fila);
   inParameters.put( "anaquel",anaquel);
   inParameters.put( "descripcion",descripcion);
   inParameters.put( "descripcion_corta",descripcionCorta);
   inParameters.put( "maquina",maquina);
    inParameters.put( "cantidad",cantidad);
     inParameters.put( "usuario",usuario);
   Map out = execute( inParameters );
   return out;
  }
}
