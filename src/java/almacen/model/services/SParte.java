package almacen.model.services;

import almacen.model.classes.CParte;
import almacen.model.classes.CParteTree;
import almacen.model.classes.CSearchParte;
import almacen.model.interfaces.IParte;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
/*
 *  @author Andrés M. Romero
 */
public class SParte extends JdbcDaoSupport implements IParte
{

    public  List<CSearchParte> search(String descripcion,String descripcionCorta)
    {

         final ArrayList<CSearchParte> results = new ArrayList<CSearchParte>();
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select t.departamento,t.linea,t.maquina,t.descripcion,t.descripcion_corta,t.cantidad,t.caja,t.posicion,t.fila,t.anaquel from fv_search_departamento t where t.descripcion like '%"+descripcion+"%' or t.descripcion_corta like '%"+descripcionCorta+"%'";
       
        template.query(QUERY,
                new RowCallbackHandler() {
                    public void processRow(ResultSet rs) throws SQLException {
                        CSearchParte parte = new CSearchParte();
                        parte.setDepartamento(rs.getString(1));
                        parte.setLinea(rs.getString(2));
                        parte.setMaquina(rs.getString(3));
                        parte.setDescripcion(rs.getString(4));
                        parte.setDescripcionCorta(rs.getString(5));
                         parte.setCantidad(rs.getString(6));
                        parte.setCaja(rs.getString(7));
                        parte.setPosicion(rs.getString(8));
                        parte.setFila(rs.getString(9));
                        parte.setAnaquel(rs.getString(10));
                        results.add(parte);
                }});
        return results;
    }

    public List<CParte> obtenerPartes(String maquina) {
        final ArrayList<CParte> results = new ArrayList<CParte>();
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select t.id_parte,t.descripcion,t.descripcion_corta ,t.cantidad,t.caja,t.posicion,t.fila,t.anaquel,t.f_c,t.f_a from fv_almacen_parte t where lpad(to_char(t.maquina),8,'0') like '"+maquina+"'";
     
        template.query(QUERY,
                new RowCallbackHandler() {
                    public void processRow(ResultSet rs) throws SQLException {
                        CParte parte = new CParte();
                        parte.setId(rs.getString(1));
                        parte.setDescripcion(rs.getString(2));
                        parte.setDescripcionCorta(rs.getString(3));
                       parte.setCantidad(rs.getString(4));
                        parte.setCaja(rs.getString(5));
                        parte.setPosicion(rs.getString(6));
                        parte.setFila(rs.getString(7));
                        parte.setAnaquel(rs.getString(8));
                        parte.setFechaCreacion(rs.getString(9));
                        parte.setFechaActualizacion(rs.getString(10));
                        results.add(parte);                   
                }});
        return results;
    }

    public List<CParteTree> obtenerPartesArbol(String maquina) {
        final ArrayList<CParteTree> results = new ArrayList<CParteTree>();
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select t.id_parte,t.descripcion from fv_almacen_parte t where t.maquina="+maquina;

        template.query(QUERY,
                new RowCallbackHandler() {
                    public void processRow(ResultSet rs) throws SQLException {
                        CParteTree parte = new CParteTree();
                        parte.setId(rs.getString(1));
                        parte.setDescripcion(rs.getString(2));
                        results.add(parte);
                }});
        return results;
    }

    public String nuevaParte(String caja, String posicion, String fila, String anaquel, String descripcion, String descripcionCorta, String maquina,String cantidad,String usuario) {
        SNuevaParte snd = new SNuevaParte(this.getDataSource());
        Map result = snd.doCall(caja, posicion, fila, anaquel, descripcion, descripcionCorta, maquina,cantidad,usuario);
        return (String) (result.get("mensaje"));
    }

    public String editarParte(String caja, String posicion, String fila, String anaquel, String descripcion, String descripcionCorta, String maquina,String oldCaja,String oldPosicion,String cantidad,String usuario) {
        SEditarParte snd = new SEditarParte(this.getDataSource());
        Map result = snd.doCall(caja, posicion, fila, anaquel, descripcion, descripcionCorta, maquina,oldCaja,oldPosicion,cantidad,usuario);
        return (String) (result.get("mensaje"));
    }

     public String borrarParte(String numero,String usuario) {
        SBorrarParte sbd = new SBorrarParte(this.getDataSource());
        Map result = sbd.doCall(numero,usuario);
        return (String) (result.get("mensaje"));
    }

    public String agregar(String caja, String posicion, String maquina,String cantidad,String usuario) {
        SAgregarCantidadParte snd = new SAgregarCantidadParte(this.getDataSource());
        Map result = snd.doCall(caja, posicion,maquina,cantidad,usuario);
        return (String) (result.get("mensaje"));
    }

    public String sacar(String caja, String posicion, String maquina,String cantidad,String usuario) {
        SSacarCantidadParte snd = new SSacarCantidadParte(this.getDataSource());
        Map result = snd.doCall(caja, posicion,maquina,cantidad,usuario);
        return (String) (result.get("mensaje"));
    }
}
