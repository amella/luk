package almacen.model.services;

import almacen.model.classes.CMaquina;
import almacen.model.classes.CMaquinaTree;
import almacen.model.interfaces.IMaquina;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
/*
 *  @author Andrés M. Romero
 */
public class SMaquina extends JdbcDaoSupport implements IMaquina {

    public List<CMaquina> obtenerMaquinas(String linea, String estado) {
        final ArrayList<CMaquina> results = new ArrayList<CMaquina>();
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select t.numero, t.descripcion,t.descripcion_corta, t.estado, t.fecha_c, t.fecha_a, t.num_partes from fv_almacen_maquina t where to_char(t.linea) like '" + linea + "' and t.estado like '" + estado + "'";
      
        template.query(QUERY,
                new RowCallbackHandler() {

                    public void processRow(ResultSet rs) throws SQLException {
                        CMaquina maquina = new CMaquina();
                        maquina.setId(rs.getString(1));
                        maquina.setDescripcion(rs.getString(2));
                        maquina.setDescripcionCorta(rs.getString(3));
                        maquina.setEstado(rs.getString(4));
                        maquina.setFechaCreacion(rs.getString(5));
                        maquina.setFechaActualizacion(rs.getString(6));
                        maquina.setTieneHijos(rs.getString(7));
                        results.add(maquina);
                    }
                });
        return results;
    }

    public List<CMaquinaTree> obtenerMaquinasArbol(String linea, String estado) {
        final ArrayList<CMaquinaTree> results = new ArrayList<CMaquinaTree>();
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select t.numero, t.descripcion, t.num_partes from fv_almacen_maquina t where t.linea like '" + linea + "' and t.estado like '" + estado + "'";

        template.query(QUERY,
                new RowCallbackHandler() {

                    public void processRow(ResultSet rs) throws SQLException {
                        CMaquinaTree maquina = new CMaquinaTree();
                        maquina.setId(rs.getString(1));
                        maquina.setDescripcion(rs.getString(2));
                        maquina.setTieneHijos(rs.getString(3));
                        results.add(maquina);
                    }
                });
        return results;
    }

    public String nuevaMaquina(String id, String descripcion, String descripcionCorta, String activa, String linea,String usuario) {
        SNuevaMaquina snd = new SNuevaMaquina(this.getDataSource());
        Map result = snd.doCall(id, descripcion, descripcionCorta, activa, linea,usuario);
        return (String) (result.get("mensaje"));

    }

    public String editarMaquina(String id, String idOld, String descripcion, String descripcionCorta, String estado,String usuario) {
        SEditarMaquina snd = new SEditarMaquina(this.getDataSource());
        Map result = snd.doCall(id, idOld, descripcion, descripcionCorta, estado,usuario);
        return (String) (result.get("mensaje"));

    }

    public String borrarMaquina(String id,String usuario) {
        SBorrarMaquina sbd = new SBorrarMaquina(this.getDataSource());
        Map result = sbd.doCall(id,usuario);
        return (String) (result.get("mensaje"));

    }
}
