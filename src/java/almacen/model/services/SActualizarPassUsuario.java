package almacen.model.services;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/*
 *  @author Andrés M. Romero
 */
public class SActualizarPassUsuario extends StoredProcedure
{
 public static final String PROC_NAME = "sp_actualiza_usuario";

 public SActualizarPassUsuario(DataSource ds)
  {
   setDataSource(ds);
   setSql(PROC_NAME);

   declareParameter(new SqlParameter("password", Types.VARCHAR));
   declareParameter(new SqlParameter("respuesta", Types.VARCHAR));
   declareParameter(new SqlParameter("pregunta", Types.VARCHAR));
   declareParameter(new SqlParameter("usuario", Types.INTEGER));
   declareParameter(new SqlOutParameter( "mensaje",Types.VARCHAR ));
   compile();
  }

  public Map doCall(String password,String respuesta,String pregunta,String usuario)
  {
   Map inParameters = new HashMap();
   inParameters.put( "password",password);
   inParameters.put( "respuesta",respuesta);
   inParameters.put( "pregunta",pregunta);
   inParameters.put( "usuario",usuario);

   Map out = execute( inParameters );
   return out;
  }
}
