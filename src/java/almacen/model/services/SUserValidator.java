package almacen.model.services;

import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
/*
 *  @author Andrés M. Romero
 */
public class SUserValidator extends JdbcDaoSupport
{

    public boolean isValid(String user,String password)
    {
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select count(t.login) from tb_usuario t where t.login=? and t.password=?";
        return template.queryForInt(QUERY, new Object[]{user,password})!=0;
    }

    public boolean isManager(String user)
    {
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select count(t.login) from tb_usuario t where t.login=? and t.rol=1";
        return template.queryForInt(QUERY, new Object[]{user})!=0;
    }

    public boolean isPasswordNull(String user)
    {
        JdbcTemplate template = getJdbcTemplate();
        String QUERY = "select count(t.login) from tb_usuario t where t.login=? and t.password is null";
        return template.queryForInt(QUERY, new Object[]{user})!=0;
    }

    public String actualiza(String password,String respuesta,String pregunta,String usuario)
    {
        SActualizarPassUsuario sbd = new SActualizarPassUsuario(this.getDataSource());
        Map result = sbd.doCall(password, respuesta, pregunta, usuario);
        return (String) (result.get("mensaje"));
    }

}
