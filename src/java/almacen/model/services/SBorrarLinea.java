package almacen.model.services;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/*
 *  @author Andrés M. Romero
 */
public class SBorrarLinea extends StoredProcedure
{
 public static final String PROC_NAME = "sp_eliminar_linea";

 public SBorrarLinea(DataSource ds)
  {
   setDataSource(ds);
   setSql(PROC_NAME);

   declareParameter(new SqlParameter("id", Types.VARCHAR));
    declareParameter(new SqlParameter("usuario", Types.INTEGER));
   declareParameter(new SqlOutParameter( "mensaje",Types.VARCHAR ));
   compile();
  }

  public Map doCall(String id,String usuario)
  {
   Map inParameters = new HashMap();
   inParameters.put( "id",id);
  inParameters.put( "usuario",usuario);
   Map out = execute( inParameters );
   return out;
  }
}
