package almacen.model.classes;
/*
 *  @author Andrés M. Romero
 */
public class CDepartamentoTree
{
    private String id;
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
