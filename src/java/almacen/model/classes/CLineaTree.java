package almacen.model.classes;
/*
 *  @author Andrés M. Romero
 */
public class CLineaTree
{
    private String id;
    private String descripcion;
    private String tieneHijos;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTieneHijos() {
        return tieneHijos;
    }

    public void setTieneHijos(String tieneHijos) {
        this.tieneHijos = tieneHijos;
    }
    
}
