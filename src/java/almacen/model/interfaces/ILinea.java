package almacen.model.interfaces;

import almacen.model.classes.CLinea;
import almacen.model.classes.CLineaTree;
import java.util.List;
/*
 *  @author Andrés M. Romero
 */
public interface ILinea extends IContar
{
 String nuevaLinea(String id, String descripcion, String departamento,String usuario);
 String editarLinea(String id, String id_old,String descripcion,String usuario);
 String borrarLinea(String id,String usuario);
 List<CLinea> obtenerLineas(String departamento);
 List<CLineaTree> obtenerLineasArbol(String departamento);
 int contar();
}
