package almacen.model.interfaces;

import almacen.model.classes.CDepartamento;
import almacen.model.classes.CDepartamentoTree;
import java.util.List;
/*
 *  @author Andrés M. Romero
 */
public interface IDepartamento 
{
 String nuevoDepartamento(String id, String descripcion,String usuario);
 String editarDepartamento(String id,String id_old, String descripcion,String usuario);
 String borrarDepartamento(String id,String usuario);
 List<CDepartamentoTree> obtenerDepartamentosArbol();
 List<CDepartamento> obtenerDepartamentos();
 int contarDepartamentosArbol();
 int contarDepartamentos();
}
