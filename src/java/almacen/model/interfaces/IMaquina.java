package almacen.model.interfaces;

import almacen.model.classes.CMaquina;
import almacen.model.classes.CMaquinaTree;
import java.util.List;
/*
 *  @author Andrés M. Romero
 */
public interface IMaquina 
{
 String nuevaMaquina(String id,String descripcion,String descripcionCorta,String activa,String linea,String usuario);
 String editarMaquina(String id,String idOld,String descripcion,String descripcionCorta,String estado,String usuario);
 String borrarMaquina(String id,String usuario);
 List<CMaquina> obtenerMaquinas(String linea, String estado);
 List<CMaquinaTree> obtenerMaquinasArbol(String linea, String estado);
}
