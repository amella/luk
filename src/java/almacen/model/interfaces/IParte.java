package almacen.model.interfaces;

import almacen.model.classes.CParte;
import almacen.model.classes.CParteTree;
import almacen.model.classes.CSearchParte;
import java.util.List;
/*
 *  @author Andrés M. Romero
 */
public interface IParte
{
 List<CParte> obtenerPartes(String maquina);
 List<CParteTree> obtenerPartesArbol(String maquina);
 String nuevaParte(String caja, String posicion, String fila, String anaquel, String descripcion, String descripcionCorta, String maquina,String cantidad,String usuario);
 String editarParte(String caja, String posicion, String fila, String anaquel, String descripcion, String descripcionCorta, String maquina,String oldCaja,String oldPosicion,String cantidad,String usuario);
 String borrarParte(String numero,String usuario);
 String agregar(String caja, String posicion, String maquina,String cantidad,String usuario);
 String sacar(String caja, String posicion, String maquina,String cantidad,String usuario);
 List<CSearchParte> search(String descripcion,String descripcionCorta);
}
