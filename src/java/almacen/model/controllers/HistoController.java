package almacen.model.controllers;

import almacen.model.classes.CHistorico;
import almacen.model.services.SHisto;
import java.io.PrintWriter;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
/*
 *  @author Andrés M. Romero
 */
public class HistoController extends MultiActionController {

    private SHisto histoService;

    public SHisto getHistoService() {
        return histoService;
    }

    public void setHistoService(SHisto histoService) {
        this.histoService = histoService;
    }


    public ModelAndView obtener(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String limit=ServletRequestUtils.getStringParameter(request, "limit");
        String start= ServletRequestUtils.getStringParameter(request, "start");
        String usuario =ServletRequestUtils.getStringParameter(request, "usuario");
        String tipo =ServletRequestUtils.getStringParameter(request, "tipo");
        String accion =ServletRequestUtils.getStringParameter(request, "accion");
        String fechai=ServletRequestUtils.getStringParameter(request, "fechai");
        String fechaf=ServletRequestUtils.getStringParameter(request, "fechaf");

        System.out.println("limit= "+limit+"\tstart= "+start);


           List<CHistorico> historicos=histoService.obtenerHistorico(tipo, usuario, accion, fechai, fechaf, start,limit);
       int cnt=  histoService.contar(tipo, usuario, accion, fechai, fechaf);
       String salida="{totalCount:"+cnt+",histo:[";
      for(CHistorico historico:historicos)
       {

        salida+="{item:'"+historico.getItem()+"'";
        salida+=",clave:'"+historico.getClave()+"'";
        salida+=",descripcion:'"+historico.getDescripcion()+"'";
        salida+=",usuario:'"+historico.getUsuario()+"'";
        salida+=",accion:'"+historico.getAccion()+"'";
        salida += ",fecha:'" + historico.getFecha() + "'";
        salida+="},";
       }

       salida=salida.substring(0,salida.length()-1);
       if(salida.length()!=0)
         salida+="]}";
       if(salida.equals("{histo:]}"))   salida="{totalCount:'0',histo:[]}";
        response.setCharacterEncoding("UTF-8");
       response.setContentType("text/plain");
       PrintWriter out=response.getWriter();
       out.print(salida);
       out.close();
       return null;

    }

}
