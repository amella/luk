package almacen.model.controllers;

import almacen.model.classes.CMaquina;
import almacen.model.interfaces.IMaquina;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
/*
 *  @author Andrés M. Romero
 */
public class MaquinaController extends MultiActionController
{
    private IMaquina maquinaService;

    public IMaquina getMaquinaService() {
        return maquinaService;
    }

    public void setMaquinaService(IMaquina maquinaService) {
        this.maquinaService = maquinaService;
    }
 
    public ModelAndView nuevo(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
          
        String id =ServletRequestUtils.getStringParameter(request, "nmid");
        String descripcion =ServletRequestUtils.getStringParameter(request, "descripcion");
        String descripcionCorta =ServletRequestUtils.getStringParameter(request, "descripcionCorta");
        String act=ServletRequestUtils.getStringParameter(request, "activa");
        String activa=(act==null)?"I":"A";
        String linea=ServletRequestUtils.getStringParameter(request, "linea");
        HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
        String result=maquinaService.nuevaMaquina(id,descripcion,descripcionCorta,activa,linea,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        
        String id =ServletRequestUtils.getStringParameter(request, "id");
        String idOld =ServletRequestUtils.getStringParameter(request, "oldId");
        String descripcion =ServletRequestUtils.getStringParameter(request, "descripcion");
        String descripcionCorta =ServletRequestUtils.getStringParameter(request, "descripcionCorta");
        String act=ServletRequestUtils.getStringParameter(request, "activa");
        String activa=(act==null)?"I":"A";
     
        HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
        String result=maquinaService.editarMaquina(id, idOld, descripcion, descripcionCorta, activa,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }


    public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        String id =ServletRequestUtils.getStringParameter(request, "id");
 HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");

        String result=maquinaService.borrarMaquina(id,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

  

    public ModelAndView maquinasParaTabla(HttpServletRequest request, HttpServletResponse response) throws Exception{

     String linea=ServletRequestUtils.getStringParameter(request,"linea");
   String estado=ServletRequestUtils.getStringParameter(request, "estado");
 estado=(estado.equals("A"))?"ACTIVA":"INACTIVA";
  
       List<CMaquina> maquinas=this.getMaquinaService().obtenerMaquinas(linea,estado);

       String salida="{maquina:[";
      for(CMaquina maquina:maquinas)
       {

        salida+="{id:'"+maquina.getId()+"'";
        salida+=",descripcion:'"+maquina.getDescripcion()+"'";
        salida+=",corta:'"+maquina.getDescripcionCorta()+"'";
        salida+=",estado:'"+maquina.getEstado()+"'";
        salida += ",creacion:'" + maquina.getFechaCreacion() + "'";
        salida += ",actualizacion:'" + maquina.getFechaActualizacion() + "'";
        salida+=",partes:'"+maquina.getTieneHijos()+"'";
        salida+="},";
       }

       salida=salida.substring(0,salida.length()-1);
       if(salida.length()!=0)
         salida+="]}";
       if(salida.equals("{maquina:]}"))   salida="{maquina:[]}";
        response.setCharacterEncoding("UTF-8");
       response.setContentType("text/plain");
       PrintWriter out=response.getWriter();
       out.print(salida);
       out.close();
       return null;

    }

   
}
