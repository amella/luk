package almacen.model.controllers;

import almacen.model.classes.CLinea;
import almacen.model.interfaces.ILinea;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
/*
 *  @author Andrés M. Romero
 */
public class LineaController extends MultiActionController
{
    private ILinea lineaService;

    public ILinea getLineaService() {
        return lineaService;
    }

    public void setLineaService(ILinea lineaService) {
        this.lineaService = lineaService;
    }

    public ModelAndView nuevo(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String depto=ServletRequestUtils.getStringParameter(request, "departamento");
     
        String tcc =ServletRequestUtils.getStringParameter(request, "tcc");
        String descripcion =ServletRequestUtils.getStringParameter(request, "descripcion");
 HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
      
        String result=lineaService.nuevaLinea(tcc, descripcion,depto,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

public ModelAndView editar(HttpServletRequest request, HttpServletResponse response) throws Exception
    {

      
        String id =ServletRequestUtils.getStringParameter(request, "tcc");
        String oldId =ServletRequestUtils.getStringParameter(request, "oldTcc");
        String descripcion =ServletRequestUtils.getStringParameter(request, "descripcion");
 HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
        String result=lineaService.editarLinea(id, oldId, descripcion,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
       response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();

        out.print(message);
        out.close();
        return null;
    }

public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        String id =ServletRequestUtils.getStringParameter(request, "tcc");
 HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");

        String result=lineaService.borrarLinea(id,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

    
 
    public ModelAndView lineasParaTabla(HttpServletRequest request, HttpServletResponse response) throws Exception{

       String depto=ServletRequestUtils.getStringParameter(request,"departamento");
     
       List<CLinea> lineas=this.getLineaService().obtenerLineas(depto);

       String salida="{linea:[";
       for(CLinea linea:lineas)
       {       
        salida+="{tcc:'" + linea.getId() + "'";
        salida+=",descripcion:'"+linea.getDescripcion()+"'";
        salida += ",creacion:'" + linea.getFechaCreacion() + "'";
        salida += ",actualizacion:'" + linea.getFechaActualizacion() + "'";
        salida+=",maquinas:"+linea.getTieneHijos();
        salida+="},";
       }

       salida=salida.substring(0,salida.length()-1);
       if(salida.length()!=0)
         salida+="]}";
       if(salida.equals("{linea:]}"))   salida="{linea:[]}";

    
 response.setCharacterEncoding("UTF-8");
       response.setContentType("text/plain");
       PrintWriter out=response.getWriter();
       out.print(salida);
       out.flush();
       out.close();
       return null;

    }


}
