package almacen.model.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
/*
 *  @author Andrés M. Romero
 */
public class IndexController implements Controller
{
  
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
       
     
          return new ModelAndView("login");
    }

}
