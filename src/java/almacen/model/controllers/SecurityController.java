package almacen.model.controllers;

import almacen.model.services.SUserValidator;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
/*
 *  @author Andrés M. Romero
 */
public class SecurityController extends MultiActionController
{
     private SUserValidator userValidator;

    public SUserValidator getUserValidator() {
        return userValidator;
    }

    public void setUserValidator(SUserValidator userValidator) {
        this.userValidator = userValidator;
    }

    public ModelAndView check(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String pass = ServletRequestUtils.getStringParameter(request, "contrasenya");
       

        String message = "";
        if (!pass.equals("deleteSuper1")) {
            message += "{success:false, message:'Password incorrecto.'}";
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

    public ModelAndView set(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String pass = ServletRequestUtils.getStringParameter(request, "pass");
        String respuesta = ServletRequestUtils.getStringParameter(request, "respuesta");
        String sq = ServletRequestUtils.getStringParameter(request, "sq");
        String usr=ServletRequestUtils.getStringParameter(request, "eusr");
        String pregunta="1";
        if(sq.contains("mascota"))
          pregunta="2";
        else
        if(sq.contains("cantante"))
          pregunta="3";

         String result = userValidator.actualiza(pass, respuesta, pregunta, usr);

        String message = "";
        if (!result.equals("OK")) {
            message += "{success:false, message:'" + result + "'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;

    }
}
