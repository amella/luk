package almacen.model.controllers;

import almacen.model.services.SUserValidator;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
/*
 *  @author Andrés M. Romero
 */
public class LoginController implements Controller
{
   private SUserValidator userValidator;

    public SUserValidator getUserValidator() {
        return userValidator;
    }

    public void setUserValidator(SUserValidator userValidator) {
        this.userValidator = userValidator;
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
          String usuario=ServletRequestUtils.getStringParameter(request, "usuario");
          String password=ServletRequestUtils.getStringParameter(request, "password");
          String message=null;
          if(userValidator.isValid(usuario, password))
          {
             
           HttpSession session =request.getSession();
           session.setAttribute("usuario", usuario);
           session.setAttribute("password", password);
      
           if(userValidator.isManager(usuario))           
                message="{status:'true',value:'manager.htm'}";           
           else
               message="{status:'true',value:'user.htm'}";
                    
          }
          else
          if(userValidator.isPasswordNull(usuario))
          {
               message="{status:'false',value:'SET'}";
          }
          else
               message="{status:'false',value:'Verifique sus datos.'}";
 response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;

    }
}
