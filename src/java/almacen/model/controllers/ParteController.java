package almacen.model.controllers;

import almacen.model.classes.CParte;
import almacen.model.classes.CSearchParte;
import almacen.model.interfaces.IParte;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
/*
 *  @author Andrés M. Romero
 */
public class ParteController extends MultiActionController {

    private IParte parteService;

    public IParte getParteService() {
        return parteService;
    }

    public void setParteService(IParte parteService) {
        this.parteService = parteService;
    }

    public ModelAndView partesParaTabla(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String salida = "{parte:[";
        String maquina = ServletRequestUtils.getRequiredStringParameter(request, "maquina");
       
        List<CParte> partes = this.getParteService().obtenerPartes(maquina);

        
        for (CParte parte : partes) {
            salida += "{numero:'" + parte.getId() + "'";
            salida += ",descripcion:'" + parte.getDescripcion() + "'";
            salida += ",corta:'" + parte.getDescripcionCorta() + "'";
            salida += ",cantidad:'" + parte.getCantidad() + "'";
            salida += ",caja:'" + parte.getCaja() + "'";
            salida += ",posicion:'" + parte.getPosicion() + "'";
            salida += ",fila:'" + parte.getFila() + "'";
            salida += ",anaquel:'" + parte.getAnaquel() + "'";
            salida += ",creacion:'" + parte.getFechaCreacion() + "'";
            salida += ",actualizacion:'" + parte.getFechaActualizacion() + "'";

            System.out.println("f_c= "+parte.getFechaCreacion()+"\t f_a= "+parte.getFechaActualizacion());


            salida += "},";
        }

        salida = salida.substring(0, salida.length() - 1);
        if (salida.length() != 0) {
            salida += "]}";
        }
        if (salida.equals("{parte:]}")) {
            salida = "{parte:[]}";
        }
        
      
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.print(salida);
        System.out.println(salida);

        out.close();
        return null;

    }

      public ModelAndView nuevo(HttpServletRequest request, HttpServletResponse response) throws Exception
      {
          String caja=ServletRequestUtils.getStringParameter(request, "npfcaja");
          String posicion=ServletRequestUtils.getStringParameter(request, "npfposicion");
          String fila=ServletRequestUtils.getStringParameter(request, "npffila");
          String anaquel=ServletRequestUtils.getStringParameter(request, "npfanaquel");
          String descripcion=ServletRequestUtils.getStringParameter(request, "descripcion");
          String descripcionCorta=ServletRequestUtils.getStringParameter(request, "descripcionCorta");
          String maquina=ServletRequestUtils.getStringParameter(request, "maquina");
          String cantidad=ServletRequestUtils.getStringParameter(request, "npfcantidad");
         
          HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");

        String result=parteService.nuevaParte(caja, posicion, fila, anaquel, descripcion, descripcionCorta, maquina,cantidad,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

      public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) throws Exception
      {
          String caja=ServletRequestUtils.getStringParameter(request, "epfcaja");
          String posicion=ServletRequestUtils.getStringParameter(request, "epfposicion");
          String fila=ServletRequestUtils.getStringParameter(request, "epffila");
          String anaquel=ServletRequestUtils.getStringParameter(request, "epfanaquel");
          String descripcion=ServletRequestUtils.getStringParameter(request, "descripcion");
          String descripcionCorta=ServletRequestUtils.getStringParameter(request, "descripcionCorta");
          String maquina=ServletRequestUtils.getStringParameter(request, "maquina");
          String oldCaja=ServletRequestUtils.getStringParameter(request, "epfoldcaja");
          String oldPosicion=ServletRequestUtils.getStringParameter(request, "epfoldposicion");
          String cantidad=ServletRequestUtils.getStringParameter(request, "epfcantidad");
           HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
          String result=parteService.editarParte(caja, posicion, fila, anaquel, descripcion, descripcionCorta, maquina,oldCaja,oldPosicion,cantidad,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

    public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        String numero =ServletRequestUtils.getStringParameter(request, "numero");
        HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
        String result=parteService.borrarParte(numero,usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

      public ModelAndView sacar(HttpServletRequest request, HttpServletResponse response) throws Exception
      {

          String caja=ServletRequestUtils.getStringParameter(request, "caja");
          String posicion=ServletRequestUtils.getStringParameter(request, "posicion");
          String maquina=ServletRequestUtils.getStringParameter(request, "maquina");
          String cantidad=ServletRequestUtils.getStringParameter(request, "cantidad");

          HttpSession session =request.getSession();
          String usuario=(String)session.getAttribute("usuario");
          String result=parteService.sacar(caja, posicion, maquina, cantidad, usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

      public ModelAndView agregar(HttpServletRequest request, HttpServletResponse response) throws Exception
      {

          String caja=ServletRequestUtils.getStringParameter(request, "caja");
          String posicion=ServletRequestUtils.getStringParameter(request, "posicion");
         
          String cantidad=ServletRequestUtils.getStringParameter(request, "cantidad");
           String maquina=ServletRequestUtils.getStringParameter(request, "maquina");

           HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
          String result=parteService.agregar(caja, posicion, maquina, cantidad, usuario);

        String message="";
        if(!result.equals("OK"))

        {
          message+="{success:false, message:'"+result+"'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

        public ModelAndView search(HttpServletRequest request, HttpServletResponse response) throws Exception
      {
          String salida = "{parte:[";
          String d=ServletRequestUtils.getStringParameter(request, "d");
          String c=ServletRequestUtils.getStringParameter(request, "c");
 
          d=d.trim();
          c=c.trim();
                  
          if((d.length()==0)&&(c.length()==0))
          {
              salida = "{parte:[]}";
          }
          else
          {

           d=d.toUpperCase();
           c=c.toUpperCase();

           if(d.length()==0)
              d=c;
           else
               if(c.length()==0)
                 c=d;
        List<CSearchParte> partes = this.getParteService().search(d,c);


        for (CSearchParte parte : partes) {
            salida += "{departamento:'" + parte.getDepartamento() + "'";
            salida += ",linea:'" + parte.getLinea() + "'";
            salida += ",maquina:'" + parte.getMaquina() + "'";
            salida += ",descripcion:'" + parte.getDescripcion() + "'";
            salida += ",corta:'" + parte.getDescripcionCorta() + "'";
            salida += ",cantidad:'" + parte.getCantidad() + "'";
            salida += ",caja:'" + parte.getCaja() + "'";
            salida += ",posicion:'" + parte.getPosicion() + "'";
            salida += ",fila:'" + parte.getFila() + "'";
            salida += ",anaquel:'" + parte.getAnaquel() + "'";


            salida += "},";
        }

        salida = salida.substring(0, salida.length() - 1);
        if (salida.length() != 0) {
            salida += "]}";
        }

        if (salida.equals("{parte:]}")) {
            salida = "{parte:[]}";
        }

          }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.print(salida);
        out.close();
        return null;
    }
}

