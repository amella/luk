package almacen.model.controllers;

import almacen.model.services.SUserValidator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
/*
 *  @author Andrés M. Romero
 */
public class ManagerController implements Controller
{
    private SUserValidator userValidator;

    public SUserValidator getUserValidator()
    {
        return userValidator;
    }

    public void setUserValidator(SUserValidator userValidator)
    {
        this.userValidator = userValidator;
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
          HttpSession session =request.getSession();
          ModelAndView mav=null;
          String usuario=(String)session.getAttribute("usuario");
          String password=(String)session.getAttribute("password");
          if((usuario==null)||(password==null))
            response.sendRedirect("index.htm");
          else
          if(userValidator.isValid(usuario, password))
          {
              mav=new ModelAndView("manager");
          }         
          else
              response.sendRedirect("index.htm");
              
          return mav;
    }

}
