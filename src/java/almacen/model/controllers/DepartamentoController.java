package almacen.model.controllers;

import almacen.model.classes.CDepartamento;
import almacen.model.interfaces.IDepartamento;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
/*
 *  @author Andrés M. Romero
 */
public class DepartamentoController extends MultiActionController {

    private IDepartamento departamentoService;

    public IDepartamento getDepartamentoService() {
        return departamentoService;
    }

    public void setDepartamentoService(IDepartamento departamentoService) {
        this.departamentoService = departamentoService;
    }

    public ModelAndView nuevo(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id = ServletRequestUtils.getStringParameter(request, "ndfid");
        String descripcion = ServletRequestUtils.getStringParameter(request, "descripcion");
        HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
       
        String result = departamentoService.nuevoDepartamento(id, descripcion,usuario);

        String message = "";
        if (!result.equals("OK")) {
            message += "{success:false, message:'" + result + "'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

    public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String id = ServletRequestUtils.getStringParameter(request, "id");
        String descripcion = ServletRequestUtils.getStringParameter(request, "descripcion");
        String oldId = ServletRequestUtils.getStringParameter(request, "oldId");
  HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");
        String result = departamentoService.editarDepartamento(id, oldId, descripcion,usuario);

        String message = "";
        if (!result.equals("OK")) {
            message += "{success:false, message:'" + result + "'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

    public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String id = ServletRequestUtils.getStringParameter(request, "id");
  HttpSession session =request.getSession();
        String usuario=(String)session.getAttribute("usuario");

        String result = departamentoService.borrarDepartamento(id,usuario);

        String message = "";
        if (!result.equals("OK")) {
            message += "{success:false, message:'" + result + "'}";
        }
         response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();


        out.print(message);
        out.close();
        return null;
    }

    public ModelAndView departamentosParaTabla(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<CDepartamento> departamentos = this.getDepartamentoService().obtenerDepartamentos();
 
        String salida = "{departamento:[";
        for (CDepartamento departamento : departamentos) {
            salida += "{id:'" + departamento.getId() + "'";
            salida += ",descripcion:'" + departamento.getDescripcion() + "'";
            salida += ",creacion:'" + departamento.getFechaCreacion() + "'";
            salida += ",actualizacion:'" + departamento.getFechaActualizacion() + "'";
            salida += ",lineas:" + departamento.getTieneHijos();
            salida += "},";
        }

        salida = salida.substring(0, salida.length() - 1);
        if (salida.length() != 0) {
            salida += "]}";
        }

      response.setCharacterEncoding("UTF-8");

        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.print(salida);
        out.close();
        return null;
    }
}
