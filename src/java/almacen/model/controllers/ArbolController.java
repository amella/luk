package almacen.model.controllers;
   
import almacen.model.classes.CDepartamentoTree;
import almacen.model.classes.CLineaTree;
import almacen.model.classes.CMaquinaTree;
import almacen.model.classes.CParteTree;
import almacen.model.interfaces.IDepartamento;
import almacen.model.interfaces.ILinea;
import almacen.model.interfaces.IMaquina;
import almacen.model.interfaces.IParte;
import java.io.PrintWriter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
/*
 *  @author Andrés M. Romero
 */
public class ArbolController implements Controller {

    private static final String MACHINE_PATTERN = "\\d{8}";
    private static final String DEPARTAMENT_PATTERN = "\\w{1,2}";
    private static final String PART_PATTERN = "\\d{11}";
    private static final String SEPARATOR = "   ";
    private IDepartamento departamentoService;
    private ILinea lineaService;
    private IMaquina maquinaService;
    private IParte parteService;

    public IParte getParteService() {
        return parteService;
    }

    public void setParteService(IParte parteService) {
        this.parteService = parteService;
    }

    public IDepartamento getDepartamentoService() {
        return departamentoService;
    }

    public void setDepartamentoService(IDepartamento departamentoService) {
        this.departamentoService = departamentoService;
    }

    public ILinea getLineaService() {
        return lineaService;
    }

    public void setLineaService(ILinea lineaService) {
        this.lineaService = lineaService;
    }

    public IMaquina getMaquinaService() {
        return maquinaService;
    }

    public void setMaquinaService(IMaquina maquinaService) {
        this.maquinaService = maquinaService;
    }

    private String departamentos() {
        List<CDepartamentoTree> departamentos = this.getDepartamentoService().obtenerDepartamentosArbol();
        String salida = "[";
        for (CDepartamentoTree departamento : departamentos) {
            salida += "{id:'" + departamento.getId() + "'";
            salida += ",text:'" + departamento.getId() + SEPARATOR + departamento.getDescripcion() + "'";
            salida += ",leaf:false";
            salida += "},";
        }

        salida = salida.substring(0, salida.length() - 1);

        if (salida.length() != 0) {
            salida += "]";
        }
        return salida;
    }

    private String lineas(String departamento) {
        List<CLineaTree> lineas = this.getLineaService().obtenerLineasArbol(departamento);

        String salida = "[";
        for (CLineaTree linea : lineas) {

            salida += "{id:'" + linea.getId() + "'";
            salida += ",text:'" + linea.getId() + SEPARATOR + linea.getDescripcion() + "'";
            salida += ",leaf:" + (((linea.getTieneHijos()).equals("0")) ? "true" : "false");
            salida += "},";
        }

        salida = salida.substring(0, salida.length() - 1);
        if (salida.length() != 0) {
            salida += "]";
        }

        return salida;
    }

    private String maquinas(String linea) {
        List<CMaquinaTree> maquinas = this.getMaquinaService().obtenerMaquinasArbol(linea, "%");

        String salida = "[";
        for (CMaquinaTree maquina : maquinas) {
            salida += "{id:'" + maquina.getId() + "'";
            salida += ",text:'" + maquina.getId() + SEPARATOR + maquina.getDescripcion() + "'";
            salida += ",leaf:" + (((maquina.getTieneHijos()).equals("0")) ? "true" : "false");
            salida += "},";
        }

        salida = salida.substring(0, salida.length() - 1);
        if (salida.length() != 0) {
            salida += "]";
        }

        return salida;
    }

    private String partes(String maquina) {
        List<CParteTree> partes = this.getParteService().obtenerPartesArbol(maquina);

        String salida = "[";
        for (CParteTree parte : partes) {

            salida += "{id:'" + parte.getId() + "'";
            salida += ",text:'" + parte.getId() + SEPARATOR + parte.getDescripcion() + "'";
            salida += ",leaf:true},";
        }

        salida = salida.substring(0, salida.length() - 1);
        if (salida.length() != 0) {
            salida += "]";
        }
        return salida;
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String node = ServletRequestUtils.getStringParameter(request, "node");

        String salida = "";

        if (node.equals("main")) {
            salida = this.departamentos();
        } else {
            Pattern p = Pattern.compile(MACHINE_PATTERN);
            Matcher m = p.matcher(node);
            if (m.matches()) {
                salida = this.partes(node);
            } else {
                p = Pattern.compile(DEPARTAMENT_PATTERN);
                m = p.matcher(node);
                if (m.matches()) {
                    salida = this.lineas(node);
                } else {
                    salida = this.maquinas(node);
                }
            }


        }

 response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
       
        out.print(salida);
        out.close();
        return null;
    }
}
